# Celestial Combat
Made in Unity using C#.

A PvP game where each player controls a ship orbiting around a black hole with gravity pulling them towards it.
Each ship is equipped with a "Tron" trail, as well as up to 4 selectable abilities.

# Controls
Main menu is controlled with mouse and keyboard, gamepads are needed on the ability select menu and for gameplay.

- Move:         Left Stick
- Aim:          Right Stick
- Abilities:    Face Buttons

## Team
People have generally worked a bit on everything, but these are their primary areas:
### Eirik Hiis-Hauge
- Gameplay
- Systems
- Player Abilities
- Ability Select Menu
- Particle System
### Miriam Tandberg Dybing
- Player Abilities
### Nataniel Gåsøy
- Main Menu
### Erlend Vetrhus Hammerstad
- Various Assets

## Build
A build can be found as a .zip file in the Downloads section.

## Video Demo
https://youtu.be/7FEmr05-hIs

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)