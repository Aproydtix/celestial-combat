﻿using UnityEngine;
using System.Collections;

public class TrailScript : MonoBehaviour {

    public int _player;             //Player ID
    public float _lifeSpan;         //Lifespan
    float _timer;                   //Timer
    float _activationTime = .05f;   //Activation time
    public GameObject _parent;      //parent

	// Use this for initialization
	void Start ()
    {
        GetComponent<SpriteRenderer>().enabled = false;     //Disable Renderer
#if UNITY_EDITOR
        if (true)                                           //Enable for debugging in Unity Editor
        {
            Color _color = GameObject.Find("Main Camera").GetComponent<MainScript>()._playerColor[_player]; //Get color
            _color.a = .5f;                                 //Set Alpha
            GetComponent<SpriteRenderer>().color = _color;  //Set color
            GetComponent<SpriteRenderer>().enabled = true;  //Enable Renderer
        }
#endif
    }

    // Update is called once per physics step
    void FixedUpdate ()
    {
        
        _timer += Time.fixedDeltaTime;  //Increase timer
        if (_timer >= _activationTime)  //Activate after a set time
            GetComponent<BoxCollider2D>().enabled = true;   //Activate collider
        if (_timer >= _lifeSpan)        //Timer over lifespan
        {
            if (_parent != null)        //if parent exists
            {
                if (_parent.GetComponent<ShipScript>()) _parent.GetComponent<ShipScript>()._trailList.Remove(gameObject);       //Remove from List
                if (_parent.GetComponent<MissileScript>()) _parent.GetComponent<MissileScript>()._trailList.Remove(gameObject); //Remove from List
            }
            Destroy(gameObject);        //Destroy Self
        }
	}
}
