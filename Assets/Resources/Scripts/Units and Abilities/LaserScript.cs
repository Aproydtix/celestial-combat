﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {

    public int _player;             //Player ID
    float _duration = .1f;          //Duration
    MainScript _mainScript;         //MainScript

    // Use this for initialization
    void Start ()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>(); //Set MainScript
    }
	
	// Update is called once per frame
    void Update()
    {
        if (_mainScript._nyan)                                              //if Nyan
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;  //Set Color
    }

	void FixedUpdate ()
    {
        transform.position = _mainScript._playerShips[_player].transform.position           //Set position to player
            + new Vector3(                                                                  //+ (
                _mainScript._playerShips[_player].GetComponent<Rigidbody2D>().velocity.x,   //player direction x
                _mainScript._playerShips[_player].GetComponent<Rigidbody2D>().velocity.y,   //player direction y
                0                                                                           //0
                ).normalized * 1.8f;                                                        //).normalized
        transform.eulerAngles = new Vector3(0, 0, _mainScript._playerShips[_player].transform.eulerAngles.z);   //Set rotation
        _duration -= Time.fixedDeltaTime;                                   //Reduce duration
        if (_duration <= 0 || !_mainScript._playerShips[_player].active)    //if duration expired or playership is dead
            Destroy(gameObject);                                            //Destroy
	}
}
