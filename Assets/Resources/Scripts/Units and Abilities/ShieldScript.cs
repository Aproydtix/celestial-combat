﻿using UnityEngine;
using System.Collections;

public class ShieldScript : MonoBehaviour {

    public int _player;         //Player ID
    public float _duration;     //Duration
    MainScript _mainScript;     //MainScript

	// Use this for initialization
	void Start ()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>(); //Set MainScript
	}

    void Update()
    {
        GetComponent<SpriteRenderer>().color = _mainScript._playerColor[_player]; //Set Color
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        _duration -= Time.fixedDeltaTime;                                   //Reduce duration
        transform.parent = _mainScript._playerShips[_player].transform;     //Set parent
        transform.position = _mainScript._playerShips[_player].transform.position;  //Set position
        if (_duration <= 0 || !_mainScript._playerShips[_player].active)    //if duration expires or player is dead
        {
            _mainScript._playerShips[_player].GetComponent<ShipScript>()._shielded = false; //player is not shielded
            Destroy(gameObject);                                            //Destroy self
        }
	}
}
