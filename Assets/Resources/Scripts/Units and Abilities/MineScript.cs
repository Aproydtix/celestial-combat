﻿using UnityEngine;
using System.Collections;

public class MineScript : MonoBehaviour {

    public int _player;                 //Player ID
    public bool _active;                //if active
    MainScript _mainScript;             //MainScript
    public float _duration = 45f;       //Duration

    // Use this for initialization
    void Start ()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();    //Set MainScript
        GetComponent<SpriteRenderer>().color = _mainScript._playerColor[_player];   //Set Color
	}

    void Update()
    {
        Color _temp = _mainScript._playerColor[_player] * (.75f + Mathf.Cos(Time.timeSinceLevelLoad * 4) / 4f); //Pulsing Color
        _temp.a = 1;                                    //Set Alpha to 1
        GetComponent<SpriteRenderer>().color = _temp;   //Set color
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        float _dis = 9001;                      //"Infinite" distance
        Vector3 _target = transform.position;   //MoveTo Target
        for (int i=0; i<4; i++)                                         //for each player
        {
            if (_mainScript._playerShips[i] != null && i != _player)    //if player active and not own player
            {
                if (Vector3.Distance(transform.position, _mainScript._playerShips[i].transform.position) < 2 && Vector3.Distance(transform.position, _mainScript._playerShips[i].transform.position) < _dis)
                {                                                                                                   //if distance < 2 and dis
                    _dis = Vector3.Distance(transform.position, _mainScript._playerShips[i].transform.position);    //Set dis
                    _target = _mainScript._playerShips[i].transform.position;                                       //Set target
                }
            }
        }
        transform.position = Vector3.MoveTowards(transform.position, _target, Time.fixedDeltaTime);         //Move to target

        _duration -= Time.fixedDeltaTime;                                                                   //Reduce duration
        transform.eulerAngles = new Vector3(0,0, transform.eulerAngles.z + Time.fixedDeltaTime*40f);        //Rotate
        if (_duration <= 0)                                                                                 //if duration expires
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)                  
                _mainScript._gameObjectScript.Sparks(transform.position, Vector2.zero, 1f, .7f, .2f, 4f);   //Create Sparks
            Destroy(gameObject);                                                                            //Destroy self
        }
    }

    void OnCollisionExit2D(Collision2D _other)
    {
        if (_other.gameObject.tag == "Player")      //if leaving player
        {
            _active = true;                         //activate self
        }
    }

    void OnCollisionEnter2D(Collision2D _other)     //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
        PlayerCrash(_other);                        //Creates Sparks, kills you, etc...
        MissileCrash(_other);                       //Creates Sparks, kills you, etc...
    }

    void OnCollisionStay2D(Collision2D _other)      //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
        PlayerCrash(_other);                        //Creates Sparks, kills you, etc...
        MissileCrash(_other);                       //Creates Sparks, kills you, etc...
    }

    void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.tag == "Laser")                  //if hit by laser
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 6; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, Vector3.zero, 1f, .7f, .2f, 4f); //Create Sparks
            Destroy(gameObject);                    //Destroy self
        }
    }

    void PlanetCrash(Collision2D _other)
    {
        if (_other.gameObject.tag == "Celestial")   //Other object is a Celestial Body
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, Vector2.zero, 1f, .7f, .2f, 4f); //Create Sparks
            Destroy(gameObject);                    //Destroy self
        }
    }

    void MissileCrash(Collision2D _other)
    {
        if (_other.gameObject.GetComponent<MissileScript>())  //Other object is a missile
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, Vector2.zero, 1f, .7f, .2f, 4f); //Create Sparks
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(_other.transform.position, _other.gameObject.GetComponent<Rigidbody2D>().velocity * 1.5f, 1f, .7f, .2f, 2f); //Create Sparks
            Destroy(gameObject);                                //Destroy self
            _mainScript._gameObjectScript.Death(_other.gameObject, _other.gameObject.GetComponent<ShipScript>()._player, true, ref _other.gameObject.GetComponent<ShipScript>()._trailList); //Destroy ship and trail
        }
    }

    void PlayerCrash(Collision2D _other)
    {
        if (!_active) return;                   //Return if not active
        if (_other.gameObject.tag == "Player")  //Other object is a player
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, Vector2.zero, 1f, .7f, .2f, 4f); //Create Sparks
            if (!_other.transform.GetComponent<ShipScript>()._shielded)     //not shielded
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                    _mainScript._gameObjectScript.Sparks(transform.position, _other.transform.GetComponent<Rigidbody2D>().velocity, 1f, .7f, .2f, 2f); //Create Sparks
            }

            if (_other.gameObject.GetComponent<ShipScript>()._invincible <= 0 && !_other.transform.GetComponent<ShipScript>()._shielded) //not invincible and not shielded
            {
                _mainScript._gameObjectScript.Death(_other.gameObject, _other.gameObject.GetComponent<ShipScript>()._player, false, ref _other.gameObject.GetComponent<ShipScript>()._trailList); //Destroy ship and trail
                if (_other.gameObject.GetComponent<ShipScript>()._player != _player) _mainScript._score[_player] += 1;                                       //Increase your score
            }

            Destroy(gameObject);                //Destroy self
        }
    }

    void OnDestroy()
    {
        GameObject _explosion = new GameObject();                                                           //Create object
        _explosion = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/MissileExplodeSprite"));   //Instantiate
        _explosion.name = "Explosion";                                                                      //Set Name
        _explosion.transform.position = transform.position;                                                 //Set position
        _explosion.transform.localEulerAngles = new Vector3(0, 0, Random.Range(0f, 360f));                  //Randomize rotation
        _explosion.transform.localScale = Vector3.one * Random.Range(0.5f, .75f);                           //Randomize size
    }
}
