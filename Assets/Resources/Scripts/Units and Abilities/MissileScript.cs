﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;

public class MissileScript : MonoBehaviour {
    public int _player;                         //Player Number
    MainScript _mainScript;                     //MainScript
    GamePad.Index _gamePadIndex;                //GamePadIndex
    float _gamePadDeadZone = .1f;               //GamePadDeadZone
    public Vector2 _engineVelocity;             //Velocity caused by engine
    Vector2 _prevPos;                           //The ships previous position, used for trail calculation
    bool _outOfBounds;                          //If player is out of bounds
    public bool _trail;                         //If the missile has a trail
    bool _active;                               //Whether the missile is active (has left the firing ship)
    float _curLifespan;                         //Current duration
    float _lifespan = 10f;                      //Duration for missiles
    public List<GameObject> _trailList = new List<GameObject>(); //List of trail objects
    GameObject _trailPool;                      //Pool of trail objects
    public float _stunned;                      //Stunned
    public int _stunnedBy;                      //Source of stun

    // Use this for initialization
    void Start () {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();            //Find MainScript
        GetComponent<SpriteRenderer>().color = _mainScript._playerColor[_player];           //Set color
        if (_trail)                                                                             //if trail
        {
            GetComponent<TrailRenderer>().material.color = _mainScript._playerColor[_player];   //Set color of trail
            GetComponent<TrailRenderer>().time = _mainScript._trailLifeSpan * 1.1f;             //Set trail length, slightly longer to sync with collision
            GetComponent<TrailRenderer>().startWidth = _mainScript._trailWidth * .75f;          //Set trail width
            GetComponent<TrailRenderer>().endWidth = _mainScript._trailWidth * .75f;            //Set trail width

            if (GameObject.Find(name + " - Trail Pool") == null)    //if pool doesn't exist
            {
                _trailPool = new GameObject();                      //Create
                _trailPool.name = name + " - Trail Pool";           //Set name
            }
            else                                                    //else
                _trailPool = GameObject.Find(name + " - Trail Pool"); //set variable
        }
        else                                                    //if not trail
        {
            GetComponent<TrailRenderer>().enabled = false;      //disable trail
            transform.localScale = transform.localScale * 1.5f; //increase scale
        }

        switch (_player)                                        //Player gamepad input
        {
            case 0: _gamePadIndex = GamePad.Index.One; break;   //Player 1
            case 1: _gamePadIndex = GamePad.Index.Two; break;   //Player 2
            case 2: _gamePadIndex = GamePad.Index.Three; break; //Player 3
            case 3: _gamePadIndex = GamePad.Index.Four; break;  //Player 4
        }
        _prevPos = transform.position;                          //Set previous position to starting position
        _curLifespan = _lifespan;
    }

    void Update()
    {
        //Set Color
        float _temp = 0;    //variable set only to use functions
        GetComponent<SpriteRenderer>().color = _mainScript._gameObjectScript.SetColor(_player, _outOfBounds, ref _temp, ref _stunned);          //Set Color
        GetComponent<TrailRenderer>().material.color = _mainScript._gameObjectScript.SetColor(_player, _outOfBounds, ref _temp, ref _stunned);  //Set Color
    }

    // Update is called once per physics step
    void FixedUpdate ()
    {
        //Movement Input and Out of Bounds
        _mainScript._gameObjectScript.MoveInput(transform, _gamePadIndex, GamePad.Axis.RightStick, ref _outOfBounds, _trail, _curLifespan, _lifespan, _stunned);

        //Maxspeed
        if (GetComponent<Rigidbody2D>().velocity.magnitude > _mainScript._maxMovementSpeed*1.5f)             //Check if velocity is over maxspeed
            GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * _mainScript._maxMovementSpeed*1.5f; //Limit speed

        //Gravity
        GetComponent<Rigidbody2D>().velocity += _mainScript._gameObjectScript.Gravity(transform.position, 0);

        //Set Direction
        transform.eulerAngles = _mainScript._gameObjectScript.SetDirection(GetComponent<Rigidbody2D>().velocity);

        if (_trail)     //if trail
        {
            //Stunned
            if (_stunned > 0)
            {
                GetComponent<TrailRenderer>().enabled = false;  //Disable trail
                foreach (GameObject _g in _trailList)           
                    Destroy(_g);                                //Destroy all trail objects
                _trailList.Clear();                             //Clear list
                _stunned -= Time.fixedDeltaTime;                //Reduce timer
            }
            else
                GetComponent<TrailRenderer>().enabled = true;   //enable trail

            //Create Trail
            if (_stunned <= 0)                                                                                                              //if not stunned
                _mainScript._gameObjectScript.CreateTrail(transform.position, _prevPos, _player, ref _trailList, gameObject, _trailPool);   //Create Trail
            _prevPos = transform.position;                      //Update Previous position

            _curLifespan -= Time.fixedDeltaTime;                //Reduce lifespan
            if (_curLifespan <= 0)                              //if lifespan expired
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity / 6; i++)
                    _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
                _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy self
            }
        }
    }

    void OnTriggerEnter2D(Collider2D _other)
    {
        if (_other.tag == "Trail")                                     //Checks if the object is trail
        {                                                                           
            if (_other.GetComponent<TrailScript>()._player != _player)              //Checks that the trail isn't yours
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                    _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
                _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
            }
        }

        if (_other.GetComponent<ShieldScript>())                                    //Checks if the object has a ShieldScript
        {                                                                           //if yes, it's a shield
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
            _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
        }

        if (_other.tag == "Laser")  //if laser
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 6; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
            _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
        }

        if (_other.tag == "StunRay")    //if stunray
        {
            _stunned = 3f;              //Set stun
            _stunnedBy = _other.GetComponent<StunRayScript>()._player;  //Set stunned by
        }
    }

    void OnCollisionEnter2D(Collision2D _other)     //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
        PlayerCrash(_other);                        //Creates Sparks, kills you, etc...
        MissileCrash(_other);                       //Creates Sparks, kills you, etc...
    }

    void OnCollisionStay2D(Collision2D _other)      //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
        PlayerCrash(_other);                        //Creates Sparks, kills you, etc...
        MissileCrash(_other);                       //Creates Sparks, kills you, etc...
    }

    void OnCollisionExit2D(Collision2D _other)
    {
        if(_other.gameObject.tag == "Player")       //if leaving player
            _active = true;                         //active is true
    }

    void PlanetCrash(Collision2D _other)
    {
        if (_other.gameObject.tag == "Celestial")                                   //Other object is a Celestial Body
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity * 1.5f, 1f, .7f, .2f, 2f); //Create Sparks
            _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
        }
    }

    void PlayerCrash(Collision2D _other)
    {
        if (!_active) return;
        if (_other.gameObject.tag == "Player")                                   //Other object is a Celestial Body
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity, 1f, .7f, .2f, 2f); //Create Sparks
            if (!_other.transform.GetComponent<ShipScript>()._shielded)         //if not shielded
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                    _mainScript._gameObjectScript.Sparks(transform.position, _other.transform.GetComponent<Rigidbody2D>().velocity, 1f, .7f, .2f, 2f); //Create Sparks
            }
            _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
            if (_other.gameObject.GetComponent<ShipScript>()._invincible <= 0 && !_other.transform.GetComponent<ShipScript>()._shielded) //if not invincible and not shielded
            {
                _mainScript._gameObjectScript.Death(_other.gameObject, _other.gameObject.GetComponent<ShipScript>()._player, false, ref _other.gameObject.GetComponent<ShipScript>()._trailList); //Destroy ship and trail
                if (_other.gameObject.GetComponent<ShipScript>()._player != _player) _mainScript._score[_player] += 1;                                       //Increase your score
            }
        }
    }

    void MissileCrash(Collision2D _other)
    {
        if (_other.gameObject.GetComponent<MissileScript>())                                   //Other object is a Missile
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity * 1.5f, 1f, .7f, .2f, 2f); //Create Sparks
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 12; i++)
                _mainScript._gameObjectScript.Sparks(_other.transform.position, _other.gameObject.GetComponent<Rigidbody2D>().velocity * 1.5f, 1f, .7f, .2f, 2f); //Create Sparks
            _mainScript._gameObjectScript.Death(gameObject, _player, true, ref _trailList); //Destroy missile and trail
            _mainScript._gameObjectScript.Death(_other.gameObject, _other.gameObject.GetComponent<ShipScript>()._player, true, ref _other.gameObject.GetComponent<ShipScript>()._trailList); //Destroy missile and trail
        }
    }
}
