﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;

public class ShipScript : MonoBehaviour {

    public int _player;                         //Player Number
    MainScript _mainScript;                     //MainScript
    GamePad.Index _gamePadIndex;                //GamePadIndex
    float _gamePadDeadZone = .1f;               //GamePadDeadZone
    public Vector2 _engineVelocity;             //Velocity caused by engine
    Vector2 _prevPos;                           //The ships previous position, used for trail calculation
    public float _invincible;                   //Invincibilty from Spawn or Abilities
    public bool _shielded;                      //Shielded
    bool _outOfBounds;                          //If player is out of bounds
    public AbilitySelectScript.Abilities[] _ability = new AbilitySelectScript.Abilities[4]; //The player's chosen Abilities
    public float[] _cooldown = new float[4];    //Cooldown for each Ability
    public List<GameObject> _trailList = new List<GameObject>(); //List of trail objects
    GameObject _trailPool;                      //Pool of trail objects
    public float _boostTimer;                   //Timer on boost
    public float _phasingTimer;                 //Timer on phasing
    public float _stunned;                      //Timer on stunned
    public int _stunnedBy;                      //Source of stun

	// Use this for initialization
	public void Initialize()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();            //Find MainScript
        GetComponent<SpriteRenderer>().color = _mainScript._playerColor[_player];           //Set color
        GetComponent<TrailRenderer>().material.color = _mainScript._playerColor[_player];   //Set color of trail
        GetComponent<TrailRenderer>().time = _mainScript._trailLifeSpan*1.1f;               //Set trail length, slightly longer to sync with collision
        GetComponent<TrailRenderer>().startWidth = _mainScript._trailWidth;                 //Set trail width
        GetComponent<TrailRenderer>().endWidth = _mainScript._trailWidth;                   //Set trail width

        GameObject ms = GameObject.Find("MenuSettings");    //Find the object containing menu settings
        for (int i = 0; i < 4; i++)
        {
            _ability[i] = ms.GetComponent<AbilitySelectScript>()._abilityInfo[ms.GetComponent<AbilitySelectScript>()._ability[_player, i]]; //Set Ability
            _cooldown[i] = 0;                                   //Sets all cooldowns to 0 in case of death
        }

        switch (_player)                                        //Player gamepad input
        {
            case 0: _gamePadIndex = GamePad.Index.One; break;   //Player 1
            case 1: _gamePadIndex = GamePad.Index.Two; break;   //Player 2
            case 2: _gamePadIndex = GamePad.Index.Three; break; //Player 3
            case 3: _gamePadIndex = GamePad.Index.Four; break;  //Player 4
        }
        _prevPos = transform.position;                          //Set previous position to starting position
        _stunned = 0;                                           //Set stunned to 0
        _shielded = false;                                      //Set shielded to false
        _invincible = 0;                                        //Set invincible to 0
        _phasingTimer = 0;                                      //Set phasing to 0
        _boostTimer = 0;                                        //Set boost to 0
        GetComponent<AudioSource>().Stop();                     //Stop all sound
        foreach (Transform t in transform)                      //for each child
            Destroy(t.gameObject);                              //Destroy all children

        if (GameObject.Find(name + " - Trail Pool") == null)    //if pool doesn't exist
        {
            _trailPool = new GameObject();                      //create pool
            _trailPool.name = name + " - Trail Pool";           //Set name
        }
        else                                                    //else
            _trailPool = GameObject.Find(name + " - Trail Pool"); //find pool
    }

    void Update()
    {
        //Set Color
        if (_phasingTimer <= 0)     //if phasing
        {
            GetComponent<SpriteRenderer>().color = _mainScript._gameObjectScript.SetColor(_player, _outOfBounds, ref _invincible, ref _stunned);        //Set Color
            GetComponent<TrailRenderer>().material.color = _mainScript._gameObjectScript.SetColor(_player, _outOfBounds, ref _invincible, ref _stunned);//Set Color
        }
        else                        //else
            GetComponent<SpriteRenderer>().color = _mainScript._gameObjectScript.SetColorPhasing(_player);  //Set Color
    }

    // Update is called once per physics step
    void FixedUpdate ()
    {
        //Buttons
        bool[] _button = new bool[4];                           //Create array for input check
        _button[0] = GamePad.GetButton(GamePad.Button.A, _gamePadIndex) || GamePad.GetButton(GamePad.Button.LeftShoulder, _gamePadIndex);                       //Ability 1
        _button[1] = GamePad.GetButton(GamePad.Button.B, _gamePadIndex) || GamePad.GetButton(GamePad.Button.RightShoulder, _gamePadIndex);                      //Ability 2
        _button[2] = GamePad.GetButton(GamePad.Button.Y, _gamePadIndex) || GamePad.GetTrigger(GamePad.Trigger.LeftTrigger, _gamePadIndex) > _gamePadDeadZone;   //Ability 3
        _button[3] = GamePad.GetButton(GamePad.Button.X, _gamePadIndex) || GamePad.GetTrigger(GamePad.Trigger.RightTrigger, _gamePadIndex) > _gamePadDeadZone;  //Ability 4

        for (int i = 0; i < 4; i++)
        {
            if (_cooldown[i] > 0) _cooldown[i] -= Time.fixedDeltaTime;              //Reduce cooldown if active
            if (_button[i] && _stunned <= 0) UseAbility(i, _ability[i]._id, _ability[i]._cooldown);  //Use Ability if button pressed
        }

        //Stunned
        if (_stunned > 0)                                   //if stunned
        {
            GetComponent<TrailRenderer>().enabled = false;  //disable trail
            foreach (GameObject _g in _trailList)           //foreach object in trailpool
                Destroy(_g);                                //destroy trail
            _trailList.Clear();                             //clear list
        }
        else                                                //else
            GetComponent<TrailRenderer>().enabled = true;   //enable trail

        //Phasing
        if (_phasingTimer > 0)                                  //if phasing
        {
            GetComponent<EdgeCollider2D>().isTrigger = true;    //No collision while phasing
            GetComponent<TrailRenderer>().enabled = false;      //Disable trail
            _phasingTimer -= Time.fixedDeltaTime;               //Reduce timer
            if (_phasingTimer <= 0)                             //if timer expired
            {
                GetComponent<EdgeCollider2D>().isTrigger = false;   //Turn on collisions
                GetComponent<TrailRenderer>().enabled = true;       //Enable trail
                GetComponent<TrailRenderer>().Clear();              //Clear trail
                _prevPos = transform.position;                      //Set prevpos
            }
        }

        if (_invincible > 0)                        //if ship is invincible
            _invincible -= Time.fixedDeltaTime;     //Reduce invincibility timer
        if (_stunned > 0)                           //if stunned              
            _stunned -= Time.fixedDeltaTime;        //Reduce timer

        //Maxspeed
        if (_boostTimer > 0)                        //if boosting
        {
            _boostTimer -= Time.fixedDeltaTime;     //Reduce timer
            //Movement Input and Out of Bounds
            _mainScript._gameObjectScript.MoveInput(transform, _gamePadIndex, GamePad.Axis.LeftStick, ref _outOfBounds, _mainScript._acceleration * 2f, _stunned);
            if (GetComponent<Rigidbody2D>().velocity.magnitude < _mainScript._maxMovementSpeed / 2f)             //Add minimum speed during boost
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * (_mainScript._maxMovementSpeed / 2f); //Limit speed
            if (GetComponent<Rigidbody2D>().velocity.magnitude > _mainScript._maxMovementSpeed * 2f)             //Check if velocity is over maxspeed * 2f
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * (_mainScript._maxMovementSpeed * 2f); //Limit speed
            _mainScript._particleScript.AfterImage(transform.position, transform.eulerAngles.z, GetComponent<SpriteRenderer>().sprite, GetComponent<SpriteRenderer>().color, transform.localScale.x); //AfterImages
        }
        if (_boostTimer <= 0) //if not boosting
        {
            //Movement Input and Out of Bounds
            _mainScript._gameObjectScript.MoveInput(transform, _gamePadIndex, GamePad.Axis.LeftStick, ref _outOfBounds, _mainScript._acceleration, _stunned);
            if (GetComponent<Rigidbody2D>().velocity.magnitude > _mainScript._maxMovementSpeed)             //Check if velocity is over maxspeed
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * _mainScript._maxMovementSpeed; //Limit speed
        }
        
        //Gravity
        GetComponent<Rigidbody2D>().velocity += _mainScript._gameObjectScript.Gravity(transform.position, _invincible);

        //Set Direction
        transform.eulerAngles = _mainScript._gameObjectScript.SetDirection(GetComponent<Rigidbody2D>().velocity);

        //Create Trail
        if (_phasingTimer <= 0 && _stunned <= 0)    //if not stunned or phasing
            _mainScript._gameObjectScript.CreateTrail(transform.position, _prevPos, _player, ref _trailList, gameObject, _trailPool); //Create trail
        _prevPos = transform.position;  //Update Previous position
    }

    void OnTriggerEnter2D(Collider2D _other)
    {
        if (_invincible > 0 || _phasingTimer > 0)  //Invincibility caused by spawning or ability
            return;
        if (_shielded)                              //if shielded
        {
            if (_other.tag == "Trail")                                     //Checks if the object is a trail
                if (_other.GetComponent<TrailScript>()._player != _player)              //Checks that the trail isn't yours
                    for (int i = 0; i < _mainScript._particleScript._particleIntensity / 150; i++)  //Create Sparks
                        _mainScript._gameObjectScript.Sparks(_other.transform.position, GetComponent<Rigidbody2D>().velocity / 5f, _mainScript._playerColor[_player].r, _mainScript._playerColor[_player].g, _mainScript._playerColor[_player].b, 2f);
            return; //return if shielded
        }

        if (_other.tag == "Trail")                                      //Checks if the object is a trail
        {                                                                         
            if (_other.GetComponent<TrailScript>()._player != _player)  //Checks that the trail isn't yours
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity/6; i++)  
                    _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
                _mainScript._gameObjectScript.Death(gameObject, _player, false, ref _trailList); //Destroy ship and trail
                _mainScript._score[_other.GetComponent<TrailScript>()._player] += 1; //Increase score of killer
            }
        }

        if (_other.tag == "Laser")                                      //if laser
        {
            if (_other.GetComponent<LaserScript>()._player != _player)  //if laser is not yours
            {
                for (int i = 0; i < _mainScript._particleScript._particleIntensity / 6; i++)
                    _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity / 3f, 1f, .7f, .2f, 2f); //Create Sparks
                _mainScript._gameObjectScript.Death(gameObject, _player, false, ref _trailList); //Destroy ship and trail
                _mainScript._score[_other.GetComponent<LaserScript>()._player] += 1; //Increase score of killer
            }
        }

        if (_other.tag == "StunRay")                                        //if stunray
        {
            if (_other.GetComponent<StunRayScript>()._player != _player)    //if not yours
            {
                _stunned = 2.5f;                                            //set stun
                _stunnedBy = _other.GetComponent<StunRayScript>()._player;  //set stunned by
            }
        }
    }

    void OnCollisionEnter2D(Collision2D _other)     //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
    }

    void OnCollisionStay2D(Collision2D _other)      //Unity function for 2D physics collision
    {
        PlanetCrash(_other);                        //Creates Sparks, kills you, etc...
    }

    void PlanetCrash(Collision2D _other)
    {
        if (_invincible > 0 || _phasingTimer > 0)   //Invincibility caused by spawning or ability
            return;

        if (_other.gameObject.tag == "Celestial")   //Other object is a Celestial Body
        {
            for (int i = 0; i < _mainScript._particleScript._particleIntensity / 6; i++)
                _mainScript._gameObjectScript.Sparks(transform.position, GetComponent<Rigidbody2D>().velocity * 1.5f, 1f, .7f, .2f, 2f);   //Create Sparks
            if (_stunned > 0) _mainScript._score[_stunnedBy] += 1;  //Increase score to the player who stunned you
            else if (_mainScript._gravityInversion > 0 && _mainScript._invertedBy != _player) _mainScript._score[_mainScript._invertedBy] += 1; //Increase score to the player inverting gravity
            _mainScript._gameObjectScript.Death(gameObject, _player, false, ref _trailList); //Destroy ship and trail
        }
    }

    void UseAbility(int _slot, int _id, float _cd)
    {
        if (_cooldown[_slot] > 0)       //Checks if cooldown is active
            return;
        switch (_id)
        {
            case 1: /*Missile*/
                GameObject _missile = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Missile"));   //Create Object
                _missile.name = "Missile - Player " + (_player+1).ToString();                                   //Set name
                _missile.transform.position = transform.position;                                               //Set position
                //Set speed
                if (GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).magnitude >= _gamePadDeadZone)
                    _missile.GetComponent<Rigidbody2D>().velocity = GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).normalized * _mainScript._maxMovementSpeed;
                else
                    _missile.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * _mainScript._maxMovementSpeed;
                _missile.GetComponent<MissileScript>()._player = _player;                                       //Set player id
                break;
            case 2: /*Shield*/
                GameObject _shield = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Shield"));     //Create Object
                _shield.name = "Shield - Player " + (_player + 1).ToString();                                   //Set name
                _shield.transform.position = new Vector3(transform.position.x, transform.position.y, -1);       //Set position
                _shield.transform.parent = transform;                                                           //Set parent
                _shield.GetComponent<ShieldScript>()._player = _player;                                         //Set player id
                _shield.GetComponent<ShieldScript>()._duration = 4.5f;                                          //Set duration
                _shielded = true;                                                                               //Set shielded variable
                break;
            case 3: /*Boost*/
                _boostTimer = 2f;                                                                       //Set timer
                GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/sfx/SFX_SpeedUp");  //Set AudioClip
                GetComponent<AudioSource>().Play();                                                     //Play sound
                break;
            case 4: /*Warp*/                                                    //Filthy hack incoming due to how TrailRenderer works...
                GameObject _warp = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/WarpSprite"));  //Create Object
                _warp.transform.position = transform.position;                  //Set position
                GameObject _newPlayer = Instantiate<GameObject>(gameObject);    //Copy ship
                _newPlayer.name = name;                                         //Set name to the new Player object
                name = name + " Old Trail";                                     //Set new name to self. This object is now only there to hold its trail
                _newPlayer.transform.position = transform.position;             //Set position of player
                GetComponent<TrailRenderer>().autodestruct = true;              //Enable autodestruct on trail so the object deletes itself
                Destroy(GetComponent<EdgeCollider2D>());                        //Remove component on this trail object
                Destroy(GetComponent<Rigidbody2D>());                           //Remove component on this trail object
                Destroy(GetComponent<SpriteRenderer>());                        //Remove component on this trail object
                _newPlayer.GetComponent<ShipScript>()._trailList.Clear();       //Clear list

                if (GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).magnitude >= _gamePadDeadZone)
                {
                    _newPlayer.transform.position += new Vector3(GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).x, GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).y, 0).normalized * 2f; //Warp in new direction
                    _newPlayer.GetComponent<Rigidbody2D>().velocity = GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).normalized * GetComponent<Rigidbody2D>().velocity.magnitude;                 //Change direction of momentum
                }
                else
                    _newPlayer.transform.position += new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0).normalized * 2f;   //Warp forwards
                _newPlayer.GetComponent<ShipScript>().Initialize();                     //Initializes the new ShipScript
                for (int i = 0; i < 4; i++)
                    _newPlayer.GetComponent<ShipScript>()._cooldown[i] = GetComponent<ShipScript>()._cooldown[i];   //Makes sure the ship retains its cooldowns
                _newPlayer.GetComponent<ShipScript>()._cooldown[_slot] = _cd;           //Sets cooldown on ability
                _newPlayer.GetComponent<ShipScript>()._phasingTimer = _phasingTimer;    //Retains phasing
                _newPlayer.GetComponent<ShipScript>()._invincible = _invincible;        //Retains invincibility
                _newPlayer.GetComponent<ShipScript>()._boostTimer = _boostTimer;        //Retains boosting
                _newPlayer.GetComponent<ShipScript>()._shielded = _shielded;            //Retains shield
                Destroy(GetComponent<ShipScript>());                                    //Remove component on this trail object
                _mainScript._playerShips[_player] = _newPlayer;                         //Updates ship in mainScript
                break;
            case 5: /*Direction Change*/
                if (GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).magnitude >= _gamePadDeadZone)
                {
                    if (_boostTimer > 0) GetComponent<Rigidbody2D>().velocity = GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).normalized * _mainScript._maxMovementSpeed*2f;  //Change direction of momentum and maximize it
                    else GetComponent<Rigidbody2D>().velocity = GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).normalized * _mainScript._maxMovementSpeed;                     //Change direction of momentum and maximize it
                }
                else
                    return; //If no new direction is set, then don't initaite cooldown
                break;
            case 6: /*Gravity Inversion*/
                _mainScript._gravityInversion = 5f;     //Set timer for gravity inversion
                _mainScript._invertedBy = _player;      //Set who inverted gravity
                GameObject _gravity = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/GravityReverseSprite"));  //Create Object
                _gravity.transform.parent = transform;  //Set parent
                _gravity.GetComponent<GravityReverseAnimationScript>()._player = _player; //Set player
                break;
            case 7: /*Phasing*/
                _phasingTimer = 3f;                     //Set timer
                foreach (GameObject _g in _trailList)   //Go through trail
                    Destroy(_g);                        //Destroy trails
                _trailList.Clear();                     //Clear list
                break;
            case 8: /*Missile with Trail*/
                GameObject _missile2 = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Missile"));  //Create Object
                _missile2.name = "Missile(Trail) - Player " + (_player + 1).ToString();                         //Set name
                _missile2.transform.position = transform.position;                                              //Set position
                //Set speed
                if (GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).magnitude >= _gamePadDeadZone)
                    _missile2.GetComponent<Rigidbody2D>().velocity = GamePad.GetAxis(GamePad.Axis.RightStick, _gamePadIndex).normalized * _mainScript._maxMovementSpeed;
                else
                    _missile2.GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * _mainScript._maxMovementSpeed;
                _missile2.GetComponent<MissileScript>()._player = _player;                                      //Set player id
                _missile2.GetComponent<MissileScript>()._trail = true;                                          //Set trail
                break;
            case 9: /*Mine*/
                GameObject _mine = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Mine"));     //Create Object
                _mine.name = "Mine - Player " + (_player + 1).ToString();                                   //Set name
                _mine.transform.position = transform.position - new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0).normalized * .2f; //Set position
                _mine.GetComponent<MineScript>()._player = _player;                                         //Set player id
                break;
            case 10: /*Stun Gun*/
                GameObject _ray = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/StunRay"));  //Create Object
                _ray.name = "StunRay - Player " + (_player + 1).ToString();                                //Set name
                _ray.GetComponent<StunRayScript>()._player = _player;                                      //Set Player
                _ray.transform.position = transform.position + new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0).normalized * 2f * _ray.transform.localScale.x/.75f;
                _ray.transform.eulerAngles = new Vector3(0, 0, transform.eulerAngles.z + 90);              //Set rotation
                break;
            case 11: /*Lazorbeam*/
                GameObject _laser = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Laser"));  //Create Object
                _laser.name = "Laser - Player " + (_player + 1).ToString();                                //Set name
                _laser.GetComponent<LaserScript>()._player = _player;                                      //Set player
                _laser.transform.position = transform.position + new Vector3(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y, 0).normalized * 1.8f;
                _laser.transform.eulerAngles = new Vector3(0,0,transform.eulerAngles.z);                   //Set rotation
                break;
        }
        Debug.Log("Player_" + (_player+1).ToString() + " Ability_" + (_slot+1).ToString() + " ID_" + _id.ToString() + " CD" + _cd.ToString()); //Debug
        _cooldown[_slot] = _cd;     //Sets cooldown
    }
}
