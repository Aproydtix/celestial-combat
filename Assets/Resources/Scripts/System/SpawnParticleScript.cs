﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnParticlesScript //: MonoBehaviour
{
    Sprite _particleSpark;                  //Sprite for the NyanStar Particle
    Sprite _particleNyan;                   //Sprite for the Spark Particle
    GameObject _particle;                   //The Particle GameObject
    public int _particleIntensity = 300;    //Intensity of Particles
    public float _partTimer;                //The Interval at which particles should be spawned if spawned on a timer
    GameObject _particlePool;               //Object to collect particles under

    Dictionary<int, Queue<GameObject>> _poolDictionary = new Dictionary<int, Queue<GameObject>>(); //The Queue of Particles


    public void Initialize()
    {
        _particleIntensity = GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>()._particleIntensity;    //Set intensity
        _partTimer = _particleIntensity / 6000f;                                //Set timer based on intensity
        _particleSpark = Resources.Load<Sprite>("Visuals/Particles/Spark");     //Load Spark Sprite
        _particleNyan = Resources.Load<Sprite>("Visuals/Particles/NyanStar");   //Load Spark Sprite
        _particle = Resources.Load<GameObject>("Prefabs/Particle");             //Load Particle GameObject
        _particlePool = new GameObject();                                       //Create parent object
        _particlePool.name = "Particle Pool";                                   //Name object
        CreatePool(_particle, _particleIntensity);                              //Create pool of required Particles with size based on intensity
    }

    #region Pools
    public void CreatePool(GameObject _prefab, int _poolSize)
    {
        int _poolKey = _prefab.GetInstanceID();         //Get ID of GameObject
        if (!_poolDictionary.ContainsKey(_poolKey))     //Check for ID in Pool
        {                                               // if not then...
            _poolDictionary.Add(_poolKey, new Queue<GameObject>()); //Create Queue(Pool) with ID
            for (int i = 0; i < _poolSize; i++)                     // with size _poolSize
            {
                GameObject _newObject = GameObject.Instantiate(_prefab) as GameObject;  //With the set Object
                _newObject.transform.parent = _particlePool.transform;                  //Set parent
                _newObject.SetActive(false);                                            //Deactivate Object for later use
                _poolDictionary[_poolKey].Enqueue(_newObject);                          //Add Object to Queue
            }
        }
    }

    public void ReuseObject(GameObject _prefab, float _r, float _g, float _b, float _startAlpha, bool _fade, float _xScroll, float _yScroll, float _xScale, float _yScale, float _xScaleChange, float _yScaleChange, float _rotation, float _duration, float _waitBeforeFade, Sprite _sprite, Vector3 _target, float _zDis, bool _gravity)
    {
        int _poolKey = _prefab.GetInstanceID();         //Get Object ID
        if (_poolDictionary.ContainsKey(_poolKey))      //Check if ID exists
        {
            GameObject _objectToReuse = _poolDictionary[_poolKey].Dequeue();    //Reuse Object and remove it fro Queue
            _poolDictionary[_poolKey].Enqueue(_objectToReuse);                  //Add it to the back of the Queue again
            _objectToReuse.SetActive(true);                                     //Activate Object
            _objectToReuse.transform.eulerAngles = new Vector3(0, 0, 0);        //Reset rotation
            _objectToReuse.name = "Particle";                                   //Set Name
            ParticleScript _pScr;                                               //Script of Particle
            if (_objectToReuse.GetComponent<ParticleScript>() != null)          //Check if object has Script
            { _pScr = _objectToReuse.GetComponent<ParticleScript>(); }          //  if true, set variable
            else                                                                //  else
            { _pScr = _objectToReuse.AddComponent<ParticleScript>(); }          //  add Script and set variable
            //Sets or Resets all variables in the ParticleScript
            _pScr._r = _r;
            _pScr._g = _g;
            _pScr._b = _b;
            _pScr._xScroll = _xScroll;
            _pScr._yScroll = _yScroll;
            _pScr._xScale = _xScale;
            _pScr._yScale = _yScale;
            _pScr._xScaleChange = _xScaleChange;
            _pScr._yScaleChange = _yScaleChange;
            _pScr._duration = _duration;
            _pScr._target = _target;
            _pScr._sprite = _sprite;
            _pScr._startAlpha = _startAlpha;
            _pScr._fade = _fade;
            _pScr._waitBeforeFade = _waitBeforeFade;
            _pScr._rotation = _rotation;
            _pScr._gravity = _gravity;
            _pScr._zDis = _zDis;
            _pScr.Start();  //Initiate Script
        }
    }
    #endregion

    #region Particles
    public void AfterImage(Vector3 _position, float _rotation, Sprite _sprite, Color _color, float _scale)
    {
        ReuseObject
            (
            _particle,
            _color.r, _color.g, _color.b, .35f,         //r,g,b,a
            true,                                       //alpha fade
            0, 0,                                       //move x,y
            _scale, _scale,                             //scale x,y
            0, 0,                                       //scale change x,y
            _rotation,                                  //rotation
            .35f,                                       //duration
            .05f,                                       //wait before fade
            _sprite,                                    //sprite
            _position,                                  //position
            -1,                                         //zDisplacement
            false                                       //gravity
            );
    }

    public void ParticlesSparks(Vector3 _position, float _xDir, float _yDir, float _r, float _g, float _b)
    {
        ReuseObject
            (
            _particle,
            _r, _g, _b, 1f,     //r,g,b,a
            false,              //alpha fade
            _xDir, _yDir,       //move x,y
            1,1,                //scale x,y
            0,0,                //scale change x,y
            0,                  //rotation
            2f,                 //duration
            .25f,               //wait before fade
            _particleSpark,     //sprite
            _position,          //position
            1,                  //zDisplacement
            true                //gravity
            );
    }

    public void NyanStars()
    {
        float _distance = Random.Range(0f,6f);      //Randomize distance from center
        float _direction = Random.Range(0f,360f);   //Randomize direction
        float _size = Random.Range(.05f, .2f);      //Randomize size
        ReuseObject
            (
            _particle,
            1f, 1f, 1f, 1f,             //r,g,b,a
            true,                       //alpha fade
            0, 0,                       //move x,y
            _size, _size,               //scale x,y
            -.005f*_size, -.005f*_size, //scale change x,y
            Random.Range(0f,360f),      //rotation
            .5f,                        //duration
            .25f,                       //wait before fade
            _particleNyan,              //sprite
            new Vector3(Mathf.Cos(_direction) * _distance, Mathf.Sin(_direction) * _distance, 0), //position
            1,                          //zDisplacement
            false                       //gravity
            );
    }
    #endregion
}
