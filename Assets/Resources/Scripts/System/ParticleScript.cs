﻿using UnityEngine;
using System.Collections;

public class ParticleScript : MonoBehaviour
{

    public float _r = .8f;          //Red
    public float _g = .8f;          //Green
    public float _b = .8f;          //Blue
    public float _startAlpha = 1f;  //Start Alpha
    public float _alpha;            //Current Alpha
    public float _xScroll = 0f;     //Movement speed across X-axis
    public float _yScroll = 0f;     //Movement speed across Y-axis
    public float _xScale = 1;       //Scale of X component
    public float _yScale = 1;       //Scale of Y component
    public float _xScaleChange = .1f; //Change of _xScale over time
    public float _yScaleChange = .1f; //Change of _yScale over time
    float _xScrolling;              //Current movement across X-axis
    float _yScrolling;              //Current movement across Y-axis
    public float _duration = 1.5f;  //Time until deactivation
    float _curDur;                  //How long the particle has been active since fading started
    public Vector3 _target;         //Position that movement is centred around
    public Sprite _sprite;          //Sprite used
    public bool _fade;              //Whether the object should fade
    public float _waitBeforeFade;   //Duration before it should start fading
    float _fadeWait;                //How long the particle has been waiting to fade
    public float _rotation;         //Rotation of object
    public float _zDis;             //Position on the Z-axis
    public bool _gravity;           //Whether the particle is affected by gravity
    public Vector2 _velocity;       //The particle's velocity if affected by gravity

    MainScript _mainScript;         //MainScript

    // Use this for initialization
    public void Start()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();    //Find MainScript
        if (GetComponent<SpriteRenderer>() == null)                                 //Check for SpriteRenderer
            gameObject.AddComponent<SpriteRenderer>();                              //Add if not found
        GetComponent<SpriteRenderer>().sprite = _sprite;                            //Set Sprite
        GetComponent<SpriteRenderer>().material.color = new Color(_r, _g, _b, _startAlpha); //Set color
        transform.eulerAngles = new Vector3(0, 0, _rotation);                       //Set rotation      
        transform.position = new Vector3(_target.x, _target.y, _zDis);              //Set position
        transform.localScale = new Vector3(_xScale, _yScale, 1);                    //Set scale          
        _xScrolling = 0;                                                            //Reset in case of reuse
        _yScrolling = 0;                                                            //Reset in case of reuse
        _curDur = 0;                                                                //Reset in case of reuse
        _fadeWait = 0;                                                              //Reset in case of reuse
        if (_gravity)
        {
            _velocity = new Vector2(_xScroll, _yScroll);                            //Set starting velocity if using gravity
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)    //if game paused
        {
            if (_gravity && _mainScript._nyan)  //if affected by gravity and Nyan
            {
                GetComponent<SpriteRenderer>().material.color = new Color(  //Randomizes color
                    Random.Range(0f, 1f),                                   //Red
                    Random.Range(0f, 1f),                                   //Green
                    Random.Range(0f, 1f),                                   //Blue
                    _alpha);                                                //Alpha
            }
            return;                 //return if paused
        }

        if (_duration <= _curDur)   //Return if object has exceeded its lifespan
            return;

        if (_fade) { _alpha = _startAlpha * (1 - _curDur / _duration); }//Alpha based on duration
        else { _alpha = _startAlpha; }                                  //Static Alpha

        if (_gravity)   //if affected by gravity
        {
            //Gravity
            _velocity += _mainScript._gameObjectScript.Gravity(transform.position, 0)*5f/4f;    //Add gravity vector

            //Set Direction
            transform.eulerAngles = _mainScript._gameObjectScript.SetDirection(_velocity);      //Update rotation

            //Faux Collision
            transform.position += new Vector3 (_velocity.x, _velocity.y, _zDis) * Time.deltaTime;    //Change position based on velocity
            for (int i = 0; i < _mainScript._gravitySources.Count; i++)                                     //Faux collision check with gravity sources
            {
                if (Vector2.Distance(transform.position, _mainScript._gravitySources[i].transform.position) //If distance to source
                    < _mainScript._gravitySources[i].transform.localScale.x                                 //  is less than source scale
                    * _mainScript._gravitySources[i].GetComponent<CircleCollider2D>().radius)               //  times collider radius
                    gameObject.SetActive(false);                                                            //  deactivate particle
            }


            transform.localScale = new Vector3(                             //Scale depending on speed and duration
                _velocity.magnitude/2f,                                     //X is speed based
                (1 - _curDur / _duration)/2f,                               //Y is duration based
                1);                                                         //Z is irrelevant

            if (_mainScript._nyan)                                          //if Nyan
                GetComponent<SpriteRenderer>().material.color = new Color(      //Randomizes color
                    Random.Range(0f,1f),                                        //Red
                    Random.Range(0f,1f),                                        //Green
                    Random.Range(0f,1f),                                        //Blue
                    _alpha);                                                    //Alpha
            else                                                            //else
                GetComponent<SpriteRenderer>().material.color = new Color(      //Color changes from red to yellow to white depending on speed
                    _velocity.magnitude / ((1.25f - _r) * 6f),                  //Red
                    _velocity.magnitude / ((1.25f - _g) * 6f),                  //Green
                    _velocity.magnitude / ((1.25f - _b) * 6f),                  //Blue
                    _alpha);                                                    //Alpha
        }
        else    //if not affected by gravity
        {
            transform.position = new Vector3                                //Position
            (
            _target.x + _xScrolling,                                        //X-axis, Static position + movement
            _target.y + _yScrolling,                                        //Y-axis, Static position + movement
            _zDis                                                           //Z-axis
            );
        }
        if (_fadeWait > _waitBeforeFade)                                    //If particle has waited long enough to fade
        {
            _curDur += Time.deltaTime;                                      //Time since fading started
            if (!_gravity)                                                  //If particle affected by gravity
            {
                GetComponent<SpriteRenderer>().material.color = new Color(_r, _g, _b, _alpha); //Update color

                transform.localScale = new Vector3                          //Update scale
                    (
                        transform.localScale.x + _xScaleChange,             //Current X scale + scaleChange
                        transform.localScale.y + _yScaleChange,             //Current Y scale + scaleChange
                        transform.localScale.z                              //Z scale is irrelevant
                    );
            }
        }
        _xScrolling += (_xScroll * Time.deltaTime) / 10f;                   //Changes position by _xScroll over time
        _yScrolling += (_yScroll * Time.deltaTime) / 10f;                   //Changes position by _yScroll over time

        _fadeWait += Time.deltaTime;                                        //Increase timer
    }

    void LateUpdate()
    {
        if ((_duration <= _curDur)                                          //If duration exceeds lifespan
            ||
            ((transform.localScale.x <= 0 || transform.localScale.y <= 0) && _xScaleChange < 0)     //If scale has reached or gone beyond 0
            ||
            ((transform.localScale.x >= 0 || transform.localScale.y >= 0) && _xScaleChange > 0))    //If scale has reached or gone beyond 0
        { gameObject.SetActive(false); }                                    //Deactivate

    }
}
