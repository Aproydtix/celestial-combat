﻿using UnityEngine;
using System.Collections;
using GamepadInput;


public class AbilitySelectScript : MonoBehaviour
{
    public bool _forceStart;                                //Force-start the game
    string[] _abilityDescription = new string[4];           //Description displayed for the currently selected ability for each player
    GamePad.Index[] _gamePadIndex = new GamePad.Index[4];   //Input for each player
    int[] _abilitySlot = new int[4];                        //The selected ability slot for each player
    public int[,] _ability = new int[4,4];                  //The abilities for each player [player, abilityslot]
    float _gamePadDeadZone = .1f;                           //GamePad DeadZone
    float[] _delay = new float[4];                          //The delay on input in menus for each player
    public int _playerCount;                                //Amount of players
    public bool[] _playerActive = new bool[4];              //Check for active player
    public bool[] _playerReady = new bool[4];               //Check for ready player
    Color[] _playerColor = new Color[4];                    //Color for each player
    public int _particleIntensity = 300;                    //Intensity of Particles
    public bool _nyan = false;                              //Nyan Mode
    public bool _reset = true;                              //Reset values in Script
    public float[] _exitTimer = new float[4];               //The timer for the exit-button being held down
    public enum GameMode { Deathmatch, Survival };          //Allowed Game Modes
    public GameMode _gameMode;                              //Current Game Mode

    public struct Abilities
    {
        public int _id;                                     //ID of Ability
        public string _name;                                //Name of Ability
        public string _description;                         //Description of Ability
        public float _cooldown;                             //Cooldown of Ability
        //Constructor
        public Abilities(int id, string name, string description, float cooldown) 
        {
            _id = id;
            _name = name;
            _description = description;
            _cooldown = cooldown;
        }
    }
    public Abilities[] _abilityInfo = new Abilities[12];    //Array of all abilities
    public Texture[] _abilitySprites = new Texture[12];     //Ability Icons
    Texture _arrow;                                         //Arrow used in UI left and right on selected ability
    public Texture _box;                                    //Box around selected ability
    Texture _ready;                                         //Texture on Ready players
    public Texture[] _button = new Texture[4];              //Buttons to indicate input (A,B,Y,X)
    GameObject[] _abilityTooltip = new GameObject[4];       //The object desplaying the tooltip for each player
    GameObject[] _playerInfo = new GameObject[4];           //The object desplaying info for each player
    GameObject _startMatch;                                 //The object indicating that you can start a match

    public GameObject _mainMenu;                            //Reference to the MainMenu object

    void Start()
    {
        _gamePadIndex[0] = GamePad.Index.One;               //Player 1
        _gamePadIndex[1] = GamePad.Index.Two;               //Player 2
        _gamePadIndex[2] = GamePad.Index.Three;             //Player 3
        _gamePadIndex[3] = GamePad.Index.Four;              //Player 4
        //Player Colors
        _playerColor[0] = new Color(1f, .25f, .25f, 1f);    //Red      P1
        _playerColor[1] = new Color(.25f, .25f, 1f, 1f);    //Blue     P2
        _playerColor[2] = new Color(.25f, 1f, .25f, 1f);    //Green    P3
        _playerColor[3] = new Color(.85f, .85f, .25f, 1f);  //Yellow   P4
        //Creating all the info for each ability: ID, Name, Description, Cooldown
        _abilityInfo[0] = new Abilities(    0,      "None",                 "An empty ability slot for the extra confident.",                                                                               0);
        _abilityInfo[1] = new Abilities(    1,      "Missile",              "Shoots a missile you can control with the right stick.",                                                                       5);
        _abilityInfo[2] = new Abilities(    2,      "Shield",               "Shields you from all damaging effects. \nYou can still collide.",                                                              10);
        _abilityInfo[3] = new Abilities(    3,      "Boost",                "Boosts you forward, can be used to intercept enemies.",                                                                        10);
        _abilityInfo[4] = new Abilities(    4,      "Warp",                 "Allows you to teleport in the direction of the right stick, \nas well as changing the direction of your momentum.",            10);
        _abilityInfo[5] = new Abilities(    5,      "Direction Change",     "Instantly changes your direction with the right stick, \nand maximizes your speed.",                                           5);
        _abilityInfo[6] = new Abilities(    6,      "Gravity Inversion",    "Inverts the direction of all gravitational pulls \nfor a set time.",                                                           10);
        _abilityInfo[7] = new Abilities(    7,      "Phasing",              "Turns off your ship's “collision”, \nallowing you to move through everything. \nTrail will be removed while phasing.",         15);
        _abilityInfo[8] = new Abilities(    8,      "Trail-Missile",        "Shoots a missile. \nYou can barely change its direction with the right stick, \nbut the missile itself has a trail.",          15);
        _abilityInfo[9] = new Abilities(    9,      "Mine",                 "Deploys a mine behind you. \nThe mine will move towards nearby enemies",                                                       10);
        _abilityInfo[10] = new Abilities(   10,     "Stun Gun",             "Shoots a wide beam that removes control of \nthe affected ship(s) for a few seconds.",                                         10);
        _abilityInfo[11] = new Abilities(   11,     "Lazorbeam",            "Shoots a thin laser straight ahead.",                                                                                          5);

        for (int i=0; i<12; i++)
            _abilitySprites[i] = Resources.Load<Texture>("Visuals/UI/Ability Icons/Ability" + i.ToString());    //Load all Ability Icons
        _arrow = Resources.Load<Texture>("Visuals/UI/Ability Icons/Arrow");                                     //Load arrow
        _box = Resources.Load<Texture>("Visuals/UI/Ability Icons/Box");                                         //Load box
        _ready = Resources.Load<Texture>("Visuals/UI/Ability Icons/Ready");                                     //Load ready
        _button[0] = Resources.Load<Texture>("Visuals/UI/Xbox Buttons/A");                                      //Load A
        _button[1] = Resources.Load<Texture>("Visuals/UI/Xbox Buttons/B");                                      //Load B
        _button[2] = Resources.Load<Texture>("Visuals/UI/Xbox Buttons/Y");                                      //Load Y
        _button[3] = Resources.Load<Texture>("Visuals/UI/Xbox Buttons/X");                                      //Load X
    }

    void Update()
    {
        if (Camera.main.GetComponent<MainScript>() || (_mainMenu != null && _mainMenu.GetComponent<FindInactiveMainMenuScript>().enabled == true))
            return;                     //Return if currently playing a match (MainScript is found) or the Main Menus are still open

        if (_forceStart)                //Forces a game to start
        {
            _forceStart = false;        //Resets bool
            StartGame();                //Starts game
        }

        if (_reset)                     
            Reset();                    //Resets a bunch of variables

        float[] _xAxis = new float[4];  //X-axis check for each player
        float[] _yAxis = new float[4];  //Y-axis check for each player
        for (int i=0; i<4; i++)         //For each player
        {
            _xAxis[i] = GamePad.GetAxis(GamePad.Axis.LeftStick, _gamePadIndex[i]).x + GamePad.GetAxis(GamePad.Axis.Dpad, _gamePadIndex[i]).x;   //Joystick + D-pad
            _yAxis[i] = GamePad.GetAxis(GamePad.Axis.LeftStick, _gamePadIndex[i]).y + GamePad.GetAxis(GamePad.Axis.Dpad, _gamePadIndex[i]).y;   //Joystick + D-pad
        }

        _playerCount = 0;       //Reset for re-counting
        int _readyCount = 0;    //Count for amount of ready players
        for (int i=0; i<4; i++) //Loops through player's controllers
        {
            if (!_playerActive[i])  //if player not active
            {
                _playerInfo[i].GetComponent<GUIText>().text = "Press Start to join!\nHold B to exit";                   //Set Text
                _playerInfo[i].GetComponent<GUIText>().fontSize = Mathf.RoundToInt(30 * Camera.main.pixelWidth / 1920f);//Set font size
                _playerInfo[i].transform.position = new Vector3(.1f + i * (.8f/3f), .5f, 0);                            //Set position
                _abilityTooltip[i].SetActive(false);                            //Deactive ability tooltip

                if (GamePad.GetButton(GamePad.Button.B, _gamePadIndex[i]))      //if holding B
                    _exitTimer[i] += Time.deltaTime;                            //increase timer
                else                                                            //else
                    _exitTimer[i] = 0;                                          //reset timer
                if (_exitTimer[i] >= 1f)                                        //if timer above 1 second
                {
                    Debug.Log("Exiting Ability Select");                        //Debug
                    Reset();                                                    //Reset variables
                    _mainMenu.SetActive(true);                                  //Reactivate the Main Menu
                    _mainMenu.GetComponent<FindInactiveMainMenuScript>().enabled = true; //Reactivate Script
                    for (int j = 0; j < 4; j++)                                 //for each player
                    {
                        Destroy(_playerInfo[j]);                                //Destroy info object
                        Destroy(_abilityTooltip[j]);                            //Destroy tooltip object
                    }
                    Destroy(_startMatch);                                       //Destroy start object
                    _reset = true;                                              //Set variable
                    return;                                                     //Return
                }


                if (GamePad.GetButtonDown(GamePad.Button.Start, _gamePadIndex[i]))  //if pressing start
                    _playerActive[i] = true;                                        //Let player join

                continue;   //Goto next player
            }               //ends the playeractive if

            _playerCount++;                                                 //Increase playercount since player is active
            _abilityTooltip[i].SetActive(true);                             //Set tooltip to active

            if (GamePad.GetButtonDown(GamePad.Button.A, _gamePadIndex[i]))  //if player presses A
                _playerReady[i] = true;                                     //Player is ready
            if (GamePad.GetButtonDown(GamePad.Button.B, _gamePadIndex[i]))  //if player presses B
            {
                if (_playerReady[i])                                        //if player is ready
                    _playerReady[i] = false;                                //player is no longer ready
                else                                                        //else
                {
                    _playerActive[i] = false;                               //player is no longer active
                    continue;                                               //goto next player, as this one is no longer active
                }
            }

            if (_playerReady[i]) _readyCount++;                             //increase ready count if ready
            if (!_playerReady[i]) _playerInfo[i].GetComponent<GUIText>().text = "Press B to leave\nPress A if you're ready";    //Set text if not ready
            else _playerInfo[i].GetComponent<GUIText>().text = "Press B if you're not ready";                                   //else set text
            _playerInfo[i].GetComponent<GUIText>().fontSize = Mathf.RoundToInt(30 * Camera.main.pixelWidth / 1920f);            //Set font size
            _playerInfo[i].transform.position = new Vector3(.1f + i * (.8f / 3f), .25f, 0);                                     //Set screen position

            if (!_playerReady[i])
            {
                //Selected Ability slot
                if (Mathf.Abs(_yAxis[i]) > _gamePadDeadZone)                        //If over deadzone
                {
                    if (_delay[i] <= 0)                                             //If delay is 0 or less
                    {
                        _abilitySlot[i] -= Mathf.FloorToInt(Mathf.Sign(_yAxis[i])); //Move to next abilityslot
                        if (_abilitySlot[i] > 3) _abilitySlot[i] = 0;               //Loop if too high
                        if (_abilitySlot[i] < 0) _abilitySlot[i] = 3;               //Loop if too low
                        _delay[i] = .2f;                                            //Set delay
                    }
                }
                //Changing Ability in slot
                if (Mathf.Abs(_xAxis[i]) > _gamePadDeadZone)                        //If over deadzone
                {
                    if (_delay[i] <= 0)                                             //If delay is 0 or less
                    {
                        _ability[i, _abilitySlot[i]] += Mathf.FloorToInt(Mathf.Sign(_xAxis[i]));        //Change ability
                        bool _duplicate;                                                                //Variable to check for duplicate abilites
                        do
                        {
                            if (_ability[i, _abilitySlot[i]] > 11) _ability[i, _abilitySlot[i]] = 0;    //Loop if too high
                            if (_ability[i, _abilitySlot[i]] < 0) _ability[i, _abilitySlot[i]] = 11;    //Loop if too low
                            _duplicate = false;                                                         //Set variable before check
                            for (int j = 0; j < 4; j++)                                                 //For each abilityslot
                            {
                                if (_ability[i, _abilitySlot[i]] != 0 && j != _abilitySlot[i] && _ability[i, _abilitySlot[i]] == _ability[i, j])    //If ability is not 0, is not itself, and equal another ability
                                    _duplicate = true;                                                                                              //  then duplicate is true
                            }
                            if (_duplicate) _ability[i, _abilitySlot[i]] += Mathf.FloorToInt(Mathf.Sign(_xAxis[i]));    //If duplicate, change ability
                        } while (_duplicate);                                                                           //If duplicate, loop
                        _delay[i] = .2f;                                            //Set delay
                    }
                }
            }

            if (Mathf.Abs(_xAxis[i]) < _gamePadDeadZone && Mathf.Abs(_yAxis[i]) < _gamePadDeadZone) //If not holding in any direction
                _delay[i] = 0;                                                                      //  delay is reset
            if (_delay[i] > 0) _delay[i] -= Time.deltaTime;                                         //Reduce delay timer

            if (_ability[i, _abilitySlot[i]] == 0) _abilityTooltip[i].GetComponent<GUIText>().color = Color.red;    //if ability selected is "None", change color to red
            else _abilityTooltip[i].GetComponent<GUIText>().color = Color.white;                                    //else set color to white
            _abilityTooltip[i].GetComponent<GUIText>().text = _abilityInfo[_ability[i,_abilitySlot[i]]]._name + "\n\n" + _abilityInfo[_ability[i, _abilitySlot[i]]]._description;   //Set text to name \n\n info
            _abilityTooltip[i].GetComponent<GUIText>().fontSize = Mathf.RoundToInt(18 * Camera.main.pixelWidth/1920f); //set font size
            _abilityTooltip[i].transform.position = new Vector3(.01f+i*.25F,.45f,0);                                //set screen position
        }

        if (_readyCount >= 2 && _readyCount == _playerCount) //if two or more players are ready
        {
            _startMatch.SetActive(true);    //activate text object
            _startMatch.GetComponent<GUIText>().fontSize = Mathf.RoundToInt(40 * Camera.main.pixelWidth / 1920f); //set font size
            _startMatch.transform.position = new Vector3(.5f, .1f, 0);  //set screen position
            for (int i=0; i<4; i++)         //loop through players
            {
                if (_playerActive[i])       //if the player is active
                    if (GamePad.GetButtonDown(GamePad.Button.Start, _gamePadIndex[i])) //if player presses start 
                        StartGame();        //start game
            }
        }
        else                                //if players are not ready
            _startMatch.SetActive(false);   //deactivate text object
    }

    void StartGame()                        
    {
        for (int i = 0; i < 4; i++)         //for each player
        {
            Destroy(_playerInfo[i]);        //destroy text object
            Destroy(_abilityTooltip[i]);    //destroy text object
        }
        Destroy(_startMatch);               //destroy text object
        Application.LoadLevel(1);           //Load level
    }
    
    void OnGUI()                                    //Draw Ability Select GUI
    {
        if (Camera.main.GetComponent<MainScript>() || (_mainMenu != null && _mainMenu.GetComponent<FindInactiveMainMenuScript>().enabled == true))
            return;                                 //Return if currently playing a match (MainScript is found) or the Main Menus are still open

        float h = Camera.main.pixelHeight/1080f;    //Relative height of game window
        float w = Camera.main.pixelWidth/1920f;     //Relative width of game window
        float d = (1920 - 200*2 - 100) / 3f;        //distance between objects calculated by (screen_width - outer_edge_distance*2 - button_size) / (players - 1)
        for (int i = 0; i < 4; i++)                 //for each player
        {
            if (_playerActive[i])                   //show GUI if player is active
            {
                for (int j = 0; j < 4; j++)         //for each ability
                {
                    if (_abilitySlot[i] == j)       //if ability slot is selected
                        GUI.color = Color.white;    //color is white
                    else                            //else
                        GUI.color = Color.grey;     //color is grey
                    if (_playerReady[i]) GUI.color = new Color(.25f, .25f, .25f, 1); //if player is ready, color is set to dark grey regardless
                    GUI.DrawTexture(new Rect((200 + d * i) * w, (150 + 110 * j) * h, 100 * w, 100 * h), _abilitySprites[_ability[i, j]]); //Draw ability icon
                }
                GUI.color = _playerColor[i];        //Set color to playercolor
                if (_playerReady[i]) GUI.color = _playerColor[i] / 2f; //if the player is ready, halve color value
                GUI.DrawTexture(new Rect((200 - 5 + d * i) * w, (150 - 5 + 110 * _abilitySlot[i]) * h, 110 * w, 110 * h), _box); //draw box around selected ability
                if (!_playerReady[i])               //if player is ready
                {
                    GUI.DrawTexture(new Rect((200 + d * i) * w, (150 + 110 * _abilitySlot[i]) * h, -100 * w, 100 * h), _arrow);     //draw left arrow
                    GUI.DrawTexture(new Rect((200 + 100 + d * i) * w, (150 + 110 * _abilitySlot[i]) * h, 100 * w, 100 * h), _arrow);//draw right arrow
                }
                for (int j = 0; j < 4; j++)         //for each ability
                {
                    if (_abilitySlot[i] == j)       //if slot is selected
                        GUI.color = Color.white;    //color is white
                    else                            //else
                        GUI.color = Color.grey;     //color is grey
                    if (_playerReady[i]) GUI.color = new Color(.25f, .25f, .25f, 1); //if player is ready, color is set to dark grey regardless
                    GUI.DrawTexture(new Rect((200 - 5 + d * i) * w, (150 - 5 + 110 * j) * h, 30 * w, 30 * h), _button[j]); //draw button
                }
                GUI.color = Color.white;            //color is white
                if (_playerReady[i])                //if player is ready
                    GUI.DrawTexture(new Rect((200 - 100 + d * i) * w, (300 - 100) * h, 300 * w, 300 * h), _ready); //draw ready
            }
        }
    }

    void Awake()                        //When first created
    {
        DontDestroyOnLoad(gameObject);  //Makes sure object is loaded into new scenes
        if (GameObject.FindGameObjectsWithTag("GameController").Length >= 2)
            Destroy(gameObject);        //Destroys self if there already exists an instance of itself
    }

    void Reset()                                //Reset variables
    {
        if (Application.loadedLevel == 0)       //if in main menu
        {
            if (GameObject.Find("StartScreenPanel") != null)
                GameObject.Find("StartScreenPanel").SetActive(false);   //Deactivate StartScreen
            for (int i = 0; i < 4; i++)                                 //for each player
            {
                if (_abilityTooltip[i] == null)                         //if object doesn't already exist
                {
                    _abilityTooltip[i] = Instantiate(Resources.Load<GameObject>("Prefabs/ScoreDisplay")); //Create object
                    _abilityTooltip[i].GetComponent<GUIText>().alignment = TextAlignment.Left;  //Set text alignment
                    _abilityTooltip[i].GetComponent<GUIText>().anchor = TextAnchor.UpperLeft;   //Set text anchor
                    _abilityTooltip[i].SetActive(false);                                        //Deactivate for future use
                }

                if (_playerInfo[i] == null)                             //if object doesn't already exist
                {
                    _playerInfo[i] = Instantiate(Resources.Load<GameObject>("Prefabs/ScoreDisplay")); //Create object
                    _playerInfo[i].GetComponent<GUIText>().color = _playerColor[i];                 //set color
                }
            }
            if (_startMatch == null)                                    //if object doesn't already exist
            {
                _startMatch = Instantiate(Resources.Load<GameObject>("Prefabs/ScoreDisplay")); //Create object
                _startMatch.GetComponent<GUIText>().text = "Press Start to begin!";         //Set text
                _startMatch.SetActive(false);                                               //Deactivate for future use
            }

            for (int i = 0; i < 4; i++)         //for each player
            {                                   //Reset....
                _abilitySlot = new int[4];      //The selected ability slot
                _ability = new int[4, 4];       //The abilities for each player [player, abilityslot]
                _delay = new float[4];          //The delay before you can continue changing abilities
                _playerActive = new bool[4];    //Active players
                _playerReady = new bool[4];     //Ready players
                _exitTimer = new float[4];      //Timers for exiting
            }
        }
        _reset = false; //Set variable to false
    }
}
