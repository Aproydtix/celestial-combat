﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;

public class MainScript : MonoBehaviour {

    public int _playerCount;                                //Amount of Players
    bool[] _playerActive = new bool[4];
    public float _acceleration = .5f;                       //Acceleration of ships
    public float _maxMovementSpeed = 5;                     //Max speed for ships
    public Color[] _playerColor = new Color[4];             //Color array for players
    public List<GameObject> _gravitySources;                //List of Gravity sources
    public GameObject[] _playerShips = new GameObject[4];   //Array of ships
    public float _trailLifeSpan = 1;                        //Trail lifespan
    public float _trailWidth = 1;                           //Trail width
    float[] _respawnTimer = new float[4];                   //Respawn Timer for each player
    public float _respawnTime = 5;                          //Time before player respawns
    public float _invincible = 2.5f;                        //Time the player is invincible after respawn
    public int[] _score = new int[4];                       //Player Score
    public int[] _health = new int[4];                      //Player Health
    GameObject[] _scoreDisplay = new GameObject[4];         //ScoreDisplay Object
    public int _endState = 10;                              //Points to end the game
    public int _winner = -1;                                //Winning Player
    Texture _point;                                         //Icon for points/health
    Texture _cdFilter;                                      //Filter to show cooldown

    public SpawnParticlesScript _particleScript;            //Script for handling and spawning Particles
    public GameObjectScript _gameObjectScript;              //Script for handling various functions for objects

    public float _gravityInversion;                         //Gravity Inversion bool
    public int _invertedBy;                                 //Person inverting gravity

    public bool _nyan;                                      //NyanCat Mode
    public Color _nyanColor;                                //NyanCat Colour
    Color[] _nyanColors = new Color[6];                     //NyanCat Colour components
    float _nyanTimer;                                       //Timer for coloour change

    int _loopStart;                                         //Start position for music looping
    int _loopEnd;                                           //End position for music looping

    GameObject _pauseText;                                  //Pause Text
    GameObject _filter;                                     //Filter over active objects

    AbilitySelectScript _ass;                               //Ability Select Script, also passes on data from Main Menu

    // Use this for initialization
    void Start ()
    {
        _ass = GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>(); //Set ASS
        _playerActive = _ass._playerActive;                     //Set if players are active
        _playerCount = _ass._playerCount;                       //Set amount of players
        _nyan = _ass._nyan;                                     //Set Nyan

        //Player Colors
        _playerColor[0] = new Color(1f,     .25f,   .25f,   1f); //Red      P1
        _playerColor[1] = new Color(.25f,   .25f,   1f,     1f); //Blue     P2
        _playerColor[2] = new Color(.25f,   1f,     .25f,   1f); //Green    P3
        _playerColor[3] = new Color(.85f,   .85f,   .25f,   1f); //Yellow   P4

        SpawnCelestials(-1);                            //Spawns the Black Hole + int planets. -1 is random 0-3
        for (int i = 0; i < 4; i++)                     //for each player
        {
            _respawnTimer[i] = _respawnTime;            //Sets respawn timer
            _health[i] = _endState;                     //Set health
        }

        _particleScript = new SpawnParticlesScript();   //Creates the ParticleSystem
        _particleScript.Initialize();                   //Initializes the system
        _gameObjectScript = new GameObjectScript();     //Creates the GameObjectSystem
        _gameObjectScript.Initialize();                 //Initializes the system

        int _randomSong = Random.Range(1, 5);
        if (_nyan) GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Nyan/BGM_Nyan1");    //Nyan Mode music
        else GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Battle/BGM_Battle" + _randomSong.ToString()); //Select random BGM
        GetComponent<AudioSource>().Play();                                                     //Play Music
        float _totalSamples = GetComponent<AudioSource>().clip.samples;                         //Find sample count
        float _songLength = GetComponent<AudioSource>().clip.length;                            //Find song length
        if (_nyan)                  //Nyan song
        {
            _loopStart = Mathf.RoundToInt(_totalSamples * (4f / _songLength));                  //00:04:00
            _loopEnd = Mathf.RoundToInt(_totalSamples * ((3f * 60f + 13.5f) / _songLength));    //03:13:50
        }
        else
        {
            if (_randomSong == 1)   //Song 1
            {
                _loopStart = Mathf.RoundToInt(_totalSamples * (19f / _songLength));                 //00:19:00
                _loopEnd = Mathf.RoundToInt(_totalSamples * ((3f * 60f + 14.5f) / _songLength));    //03:14:50
            }
            if (_randomSong == 2)   //Song 2
            {
                _loopStart = Mathf.RoundToInt(_totalSamples * (12.5f / _songLength));               //00:12:50
                _loopEnd = Mathf.RoundToInt(_totalSamples * ((4f * 60f + 13f) / _songLength));      //04:13:00
            }
            if (_randomSong == 3)   //Song 3
            {
                _loopStart = Mathf.RoundToInt(_totalSamples * (13.5f / _songLength));               //00:13:50
                _loopEnd = Mathf.RoundToInt(_totalSamples * ((2f*60f+23f) / _songLength));          //02:23:00
            } 
            if (_randomSong == 4)   //Song 4
            {
                _loopStart = Mathf.RoundToInt(_totalSamples * (10f / _songLength));                 //00:10:00
                _loopEnd = Mathf.RoundToInt(_totalSamples * ((1f * 60f + 34f) / _songLength));      //01:34:00
            }
        }
        //Debug.Log(_songLength);   //Debug
        //Debug.Log(_loopEnd);      //Debug

        _point = Resources.Load<Texture>("Visuals/UI/Ability Icons/Point"); //Set Score/Health Icon
        _cdFilter = Resources.Load<Texture>("Visuals/Particles/Pixel");     //Set Filter

        //Nyan Colors
        if (_nyan)
        {
            _nyanColors[0] = Color.red;                             //Red
            _nyanColors[1] = (Color.red + Color.yellow);            //Orange
            _nyanColors[2] = Color.yellow;                          //Yellow
            _nyanColors[3] = Color.green;                           //Green
            _nyanColors[4] = Color.blue;                            //Blue
            _nyanColors[5] = (Color.blue + Color.red);              //Purple

            GameObject.Find("Background").GetComponent<SpriteRenderer>().color = new Color(.2f, .6f, 1f, 1);    //Set to blue-ish
            GameObject.Find("Space").GetComponent<SpriteRenderer>().color = new Color(.2f, .6f, 1f, 1);         //Set to blue-ish
        }

        //Pause Items
        _filter = new GameObject();                             //Create Pause Filter
        _filter.name = "Filter";                                //Set name
        _filter.transform.position = new Vector3(0, 0, -5);     //Set position
        _filter.transform.localScale = Vector3.one * 1000;      //Set size
        _filter.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Visuals/Particles/Pixel");  //Set Sprite
        _filter.GetComponent<SpriteRenderer>().material.color = Color.clear;                            //Set color
        _pauseText = Instantiate(Resources.Load<GameObject>("Prefabs/ScoreDisplay"));                   //Create Pause Text 
        _pauseText.name = "Pause Text";                                                                 //Set Name
        _pauseText.GetComponent<GUIText>().text = "\nPaused\n\n\nPress Select to return to Main Menu";  //Set Text
        _pauseText.GetComponent<GUIText>().fontSize = Mathf.RoundToInt(Camera.main.pixelHeight / 10f);  //Set Font Size
        _pauseText.gameObject.SetActive(false);                                                         //Deactivate
    }
	
    //Update
    void Update()
    {
        //Pause
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape) || GamePad.GetButtonDown(GamePad.Button.Start, GamePad.Index.Any))
        {
            if (Time.timeScale != 0)
            {
                Time.timeScale = 0;                     //Set TimeScale to 0
                _pauseText.gameObject.SetActive(true);  //Activate Text
            }
            else
            {
                Time.timeScale = 1;                     //Set TimeScale to 1
                _pauseText.gameObject.SetActive(false); //Deactivate Text
            }
        }
        if (_winner >= 0)       //if there's a winner
            Time.timeScale = 0; //Pause game
        if (Time.timeScale == 0)//if game is paused
        {
            if (_winner >= 0)   //if there's a winner
            {
                _pauseText.GetComponent<GUIText>().color = _playerColor[_winner];   //Set color
                _pauseText.GetComponent<GUIText>().text = "\nPlayer " + (_winner+1).ToString() + " Wins!\n\n\nPress Select to return to Main Menu"; //Set text
                _pauseText.gameObject.SetActive(true);  //Activate text
            }
            //Exit match
            if (Input.GetKeyDown(KeyCode.E) || GamePad.GetButtonDown(GamePad.Button.Back, GamePad.Index.Any))
            {
                ReturnToMainMenu(); //Return to Main Menu
            }
        }
        else
        {
            SpawnShips();   //Respawns ships if the ship is dead and respawntimer is done
        }

        if (Time.timeScale == 0 /*|| _winner >= 0*/)                                        //if paused
            _filter.GetComponent<SpriteRenderer>().material.color = new Color(0, 0, 0, .5f);//Set color of filter
        else                                                                                //else
            _filter.GetComponent<SpriteRenderer>().material.color = Color.clear;            //Set color of filter

        //NyanCat Mode
        if (_nyan)                                  //if nyan
        {
            _nyanTimer += (142f / 60f) * 6f/240f;    //BPM/240 * timerMax
            if (_nyanTimer >= 6) _nyanTimer -= 6;   //loop timer
            //Set Nyan Color by going through the rainbow
            _nyanColor = _nyanColors[Mathf.FloorToInt(_nyanTimer)] * (1f - (_nyanTimer - Mathf.FloorToInt(_nyanTimer))) + _nyanColors[Mathf.FloorToInt(_nyanTimer + 1) % 6] * (_nyanTimer - Mathf.FloorToInt(_nyanTimer));
            for (int i = 0; i < 4; i++)         //for each player
                _playerColor[i] = _nyanColor;   //set player color
            GameObject.Find("Ring").GetComponent<SpriteRenderer>().color = _nyanColor;  //Set color of outer ring
        }
    }

	// Update is called once per physics step
	void FixedUpdate ()
    {
        if (_nyan)                          //if Nyan
            _particleScript.NyanStars();    //Spawn Star Particles

        //Gravity Inversion
        if (_gravityInversion > 0)          //if Gravity Inverted
        {
            _gravityInversion -= Time.fixedDeltaTime;                               //Reduce timer
            for (int i = 0; i < _gravitySources.Count; i++)
                _gravitySources[i].GetComponent<GravityScript>()._inverted = true;  //Set inverted to true
        }
        else
        {
            for (int i = 0; i < _gravitySources.Count; i++)
                _gravitySources[i].GetComponent<GravityScript>()._inverted = false; //Set inverted to false
        }
        GetComponent<AudioSource>().pitch = _gravitySources[0].GetComponent<GravityScript>()._gravity/ _gravitySources[0].GetComponent<GravityScript>()._mass;  //Play music backwards if inverted

        //Music Looping
        if (GetComponent<AudioSource>().timeSamples > _loopEnd) //if over loop
        GetComponent<AudioSource>().timeSamples = _loopStart;   //loop
        if (GetComponent<AudioSource>().timeSamples < _loopStart && GetComponent<AudioSource>().pitch < 0)  //if going backwards and over loop
        GetComponent<AudioSource>().timeSamples = _loopEnd;                                                 //loop

        //Score Display
        int _ships = 0;             //Ships currently active with more health left
        for (int i = 0; i < 4; i++) //for each player
        {
            if (_score[i] >= _endState && _ass._gameMode == AbilitySelectScript.GameMode.Deathmatch) //score over/equal max
                _winner = i;                                            //Set winner
            if (_health[i] > 0 && _playerActive[i]) _ships++;           //Count active ships
        }

        if (_ships == 1 && _ass._gameMode == AbilitySelectScript.GameMode.Survival) //If there's only one ship left in survival
        {
            for (int i = 0; i < 4; i++)                                 //for each player
                if (_health[i] > 0 && _playerActive[i]) _winner = i;    //if ship still has helth left, it's the winner
        }

        //Camera Position
        Vector2 _targetPos = Vector2.zero;  //(0,0)
        float _count = 0;                   //Amount of active players * relative respawn timer
        for (int i=0; i<4; i++)  //Loop through players
        {
            if (_playerShips[i] != null)    //Check if ship is active
            {
                _targetPos += (new Vector2(_playerShips[i].transform.position.x, _playerShips[i].transform.position.y) //Adds ship position
                    + _playerShips[i].GetComponent<Rigidbody2D>().velocity/5f)  //Adds ship speed to position
                    * ((_respawnTime - _respawnTimer[i])/_respawnTime);         //Multiply all of it by relative respawn timer
                _count += ((_respawnTime - _respawnTimer[i] )/ _respawnTime);   //Add to player counter
            }
        }
        if (_count != 0)                                        //If there are active players
            _targetPos = _targetPos / _count;                   //Finds centerpoint between players
        _targetPos = (_targetPos + Vector2.zero) / 2f;          //Average between players and (0,0)
        //Move towards target with a Lerp function
        transform.position = Vector2.Lerp(transform.position, _targetPos, Vector2.Distance(transform.position, _targetPos)*Time.fixedDeltaTime);  
        transform.position = new Vector3(transform.position.x, transform.position.y, -10f); //Set Z position to -10f

        //Camera Size
        float _length = 0;                  //Furthest distance between a player and camera on the X-axis
        float _height = 0;                  //Furthest distance between a player and camera on the Y-axis
        for (int i=0; i<4; i++)             //Looks for the greatest length and height difference
        {
            if (_playerShips[i] != null)    //Check if ship is active
            {
                if (Mathf.Abs(transform.position.x - _playerShips[i].transform.position.x) > _length)   //If length is greater
                    _length = Mathf.Abs(transform.position.x - _playerShips[i].transform.position.x);   //  set length
                if (Mathf.Abs(transform.position.y - _playerShips[i].transform.position.y) > _height)   //If height is greater
                    _height = Mathf.Abs(transform.position.y - _playerShips[i].transform.position.y);   //  set height
            }
        }
        float _size = _length;          //Variable for new camera size
        if (_height * 16/9 > _length)   
            _size = _height * 16/9;     //Sets size variable depending on which is biggest of length and height
        //Sets camera size with Lerp function
        GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, 3.25f + _size / 4f, Time.fixedDeltaTime);
	}

    void SpawnShips()
    {
        for (int i = 0; i < 4; i++)                         //For amount of Players
        {
            if (!_playerActive[i] || (_ass._gameMode == AbilitySelectScript.GameMode.Survival && _health[i] <= 0)) //Checks is active or is out of lives
                continue;

            if (_playerShips[i] != null)                    //Does ship exist?
            {
                if (_playerShips[i].activeSelf == true)     //If Ship is active
                {
                    _respawnTimer[i] = 0;                   //Reset the respawn timer if true
                    continue;                               //Then go to next count in i
                }
                if (_playerShips[i].activeSelf == false)                                                    //If Ship is inactive
                {
                    _playerShips[i].transform.position = new Vector3(                                       //Evenly spaces players   
                        Mathf.Cos(Mathf.PI + Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad / 5f) * 5.5f, //  around the centre
                        Mathf.Sin(Mathf.PI + Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad / 5f) * 5.5f,
                        0);
                }
            }
                                                    
            if (_respawnTimer[i] >= _respawnTime)                                                           //If timer exceeds limit
            {
                if (_playerShips[i] == null /*|| _playerShips[i].activeSelf == false*/)
                {
                    GameObject _ship = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Ship")); //Create object
                    _ship.name = "Ship - Player " + (i + 1).ToString();                                     //Name object
                    _playerShips[i] = _ship;                                                                //Add object to list
                }
                _playerShips[i].SetActive(true);
                _playerShips[i].transform.position = new Vector3(                                           //Evenly spaces players   
                    Mathf.Cos(Mathf.PI + Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad/5f) * 5.5f,       //  around the centre
                    Mathf.Sin(Mathf.PI + Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad/5f) * 5.5f,
                    0);
                _playerShips[i].GetComponent<Rigidbody2D>().velocity = new Vector2(                         //Makes ship enter area at an angle
                    -Mathf.Sin(Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad/5f),
                    Mathf.Cos(Mathf.PI * i * 2 / (4) - Time.timeSinceLevelLoad/5f)) * _maxMovementSpeed;
                _playerShips[i].GetComponent<ShipScript>()._player = i;                                     //Sets player number to ship
                _playerShips[i].GetComponent<ShipScript>().Initialize();
                _playerShips[i].GetComponent<ShipScript>()._invincible = _invincible;                       //Sets invinicbility to 1.5 seconds
                _playerShips[i].GetComponent<TrailRenderer>().Clear();

                _respawnTimer[i] = 0;                                       //Reset the respawn timer
            }
            else
                _respawnTimer[i] += Time.fixedDeltaTime;    //Increase timer
        }
    }

    void SpawnCelestials(int _amount)
    {
        GameObject _celestial = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Celestial"));   //Spawn Black Hole
        _celestial.name = "Black Hole";                                                                     //Set name
        float _randomSize = Random.Range(.90f, 1.1f);                                                       //Randomize size
        _celestial.GetComponent<GravityScript>()._mass = _randomSize / 5f;                                  //Set mass based on size
        _celestial.transform.localScale = _celestial.transform.localScale * _randomSize;                    //Set scale based on size
        _gravitySources.Add(_celestial);                                                                    //Add to list of gravity sources

        for (int i = 0; i < ((_amount == -1) ? Random.Range(0, 4) : _amount); i++)                          //Randomizes if set to -1, chance of more planets decreases
        {
            _celestial = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/Celestial"));          //Spawn Planet
            _celestial.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Visuals/Celestial Bodies/Planet"/* + Random.Range(0,2)*/);   //Set Sprite
            _celestial.name = "Planet " + (i + 1).ToString();                                               //Set name
            _randomSize = Random.Range(.25f, .5f);                                                          //Randomize size
            _celestial.GetComponent<GravityScript>()._mass = _randomSize / 5f;                              //Set mass based on size
            _celestial.GetComponent<GravityScript>()._planet = true;                                        //Set Planet to true
            _celestial.transform.localScale = _celestial.transform.localScale * _randomSize;                //Set scale based on size
            float _disCheck;                                                                                //Variable for distance check
            do
            {
                _disCheck = 9001;                                                                           //Set variable to "infinite"
                float _distance = Random.Range(1.25f, 4.25f);                                               //Randomize distance
                float _direction = Random.Range(0f, 360f);                                                  //Randomize direction
                _celestial.transform.position = new Vector3(Mathf.Cos(_direction) * _distance, Mathf.Sin(_direction) * _distance, 0);   //Set position
                for (int j = 0; j < _gravitySources.Count; j++)                                             //For each gravity source
                {
                    if (Mathf.Abs(Vector2.Distance(_gravitySources[j].transform.position, Vector2.zero) - Vector2.Distance(_celestial.transform.position, Vector2.zero)) < _disCheck) //If distance to other source is less than variable
                        _disCheck = Mathf.Abs(Vector2.Distance(_gravitySources[j].transform.position, Vector2.zero) - Vector2.Distance(_celestial.transform.position, Vector2.zero)); //  set variable
                }
            } while (_disCheck < .5f);                                                                      //Loop if objects are too close

            _gravitySources.Add(_celestial);                                                                //Add to list of gravity sources
        }
    }

    //Stretches and Rotates Sprite to chain them together
    public void StrechSprite(GameObject _sprite, Vector3 _initialPosition, Vector3 _finalPosition, float _length, float _width)
    {
        Vector2 _centerPos = (_initialPosition + _finalPosition) / 2f;          //Find middlepoint
        _sprite.transform.position = new Vector2(_centerPos.x, _centerPos.y);   //Set position
        Vector2 _direction = _finalPosition - _initialPosition;                 //Find direction
        _direction = _direction.normalized;                                     //Normalize direction
        _sprite.transform.right = _direction;                                   //Set direction
        _sprite.transform.localScale = new Vector3(Vector2.Distance(_initialPosition, _finalPosition) * _length, _width); //Set scale
    }

    //GUI
    void OnGUI()
    {
        float h = Camera.main.pixelHeight / 1080f;  //Relative height
        float w = Camera.main.pixelWidth / 1920f;   //Relative width
        float d = 80;                               //Overall UI size
        float s = d*.8f;                            //Button Size

        //I'm not even going to comment on the rest of the GUI, it's a LOT of math relative to heights, widths, sizes, positions, etc....
        float[] ph = new float[4]; ph[0] = 10; ph[1] = 10; ph[2] = 1080-10-s; ph[3] = 1080-10-s;
        float[] pw = new float[4]; pw[0] = 10; pw[1] = 1920+10-s*5; pw[2] = 10; pw[3] = 1920+10-s*5;
        for (int i = 0; i < 4; i++)
        {
            int mh; mh = (i == 2 || i == 3) ? -1 : 1;
            int mw; mw = (i == 1 || i == 3) ? -1 : 1;
            if (_playerActive[i] && _playerShips[i] != null)
            {
                for (int j=0; j<_endState; j++)
                {
                    if ((_ass._gameMode == AbilitySelectScript.GameMode.Survival && _health[i] > j) || (_ass._gameMode == AbilitySelectScript.GameMode.Deathmatch && _score[i] > j))
                        GUI.color = _playerColor[i]; else GUI.color = new Color(.25f, .25f, .25f, 1);
                    GUI.DrawTexture(new Rect((pw[i] +s*2f-s*2f*mw + d/1.5f*mw * j) * w, (ph[i]+d*1/12 - d*1/12*mh) * h, (s/1.5f) * w, (s/1.5f) * h), _point);
                }
                GUI.color = Color.white;
                for (int j = 0; j < 4; j++)
                {
                    //Abilities
                    GUI.color = Color.white;
                    GUI.DrawTexture(new Rect((pw[i] + d*j) * w, (mh*d + ph[i]) * h, s * w, s * h), _ass._abilitySprites[_playerShips[i].GetComponent<ShipScript>()._ability[j]._id]);
                    GUI.color = new Color(0f, 0f, 0f, .75f);
                    float _percent = (_playerShips[i].GetComponent<ShipScript>()._cooldown[j] / _playerShips[i].GetComponent<ShipScript>()._ability[j]._cooldown);
                    GUI.DrawTexture(new Rect((pw[i] + d * j) * w, (mh * d + ph[i]) * h +s*h - s*h*_percent, s * w, s * h*_percent), _cdFilter);
                    GUI.color = (_playerShips[i].GetComponent<ShipScript>()._cooldown[j] > 0)? new Color(.25f, .25f, .25f, 1) : _playerColor[i];
                    GUI.DrawTexture(new Rect((pw[i] + d * j-d/20) * w, (mh*d + ph[i]-d/20) * h, (s+d/10) * w, (s+d/10) * h), _ass._box);
                    GUI.color = (_playerShips[i].GetComponent<ShipScript>()._cooldown[j] > 0) ? new Color(.25f, .25f, .25f, 1) : Color.white;
                    GUI.DrawTexture(new Rect((pw[i] + d * j - d / 20) * w, (mh * d + ph[i] - d / 20) * h, (s + d / 10) * w*(3f/11f), (s + d / 10) * h*(3f/11f)), _ass._button[j]);
                }
            }
        }
    }

    //Return to MainMenu
    void ReturnToMainMenu()
    {
        Application.LoadLevel(0);   //Load MainMenu
        _ass._reset = true;         //Reset variables on ASS
        Time.timeScale = 1;         //Set TimeScale to 1
    }
}
