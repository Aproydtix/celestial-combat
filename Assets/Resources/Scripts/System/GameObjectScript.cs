﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GamepadInput;

public class GameObjectScript : MonoBehaviour {

    float _bounds = 5f;                     //Playable area radius
    float _gamePadDeadZone = .1f;           //Gamepad Deadzone
    MainScript _mainScript;                 //MainScript

	// Use this for initialization
	public void Initialize ()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();    //Set MainScript
	}

    //Input to move the object and check if the object is out of bounds
    public void MoveInput(Transform _transform, GamePad.Index _gamePadIndex, GamePad.Axis _stick, ref bool _outOfBounds, float _acceleration, float _stunned)
    {
        if (_transform.position.magnitude > _bounds)  //Out of bounds
        {
            //Automatically move towards centre of the arena
            _transform.GetComponent<Rigidbody2D>().velocity += -new Vector2(_transform.position.x, _transform.position.y).normalized * _acceleration;
            _outOfBounds = true;                //Set bool to true
        }
        else
        {
            _outOfBounds = false;               //Set bool to false
            if (_stunned > 0) return;           //Return if stunned
            if (GamePad.GetAxis(_stick, _gamePadIndex).magnitude > _gamePadDeadZone)    //Check for deadzone
                _transform.GetComponent<Rigidbody2D>().velocity += GamePad.GetAxis(_stick, _gamePadIndex) * _acceleration;  //Engine direction
        }
    }

    //Input to move the object and check if the object is out of bounds
    public void MoveInput(Transform _transform, GamePad.Index _gamePadIndex, GamePad.Axis _stick, ref bool _outOfBounds, bool _trail, float _curLifespan, float _lifespan, float _stunned)
    {
        if (_transform.position.magnitude > _bounds)  //Out of bounds
        {
            //Automatically move towards centre of the arena
            _transform.GetComponent<Rigidbody2D>().velocity += -new Vector2(_transform.position.x, _transform.position.y).normalized * _mainScript._acceleration / 2f;
            _outOfBounds = true;                //Set bool to true
        }
        else
        {
            _outOfBounds = false;               //Set bool to false
            if (_stunned > 0) return;           //Return if stunned
            //Direction for missiles either with or without trail
            if (!_trail) _transform.GetComponent<Rigidbody2D>().velocity += GamePad.GetAxis(_stick, _gamePadIndex) * _mainScript._acceleration * 1.5f;  //Engine direction
            else if (_curLifespan / _lifespan >= .75f) _transform.GetComponent<Rigidbody2D>().velocity += GamePad.GetAxis(_stick, _gamePadIndex) * _mainScript._acceleration * (.25f + (1-_curLifespan/_lifespan)*.5f);  //Engine direction
        }
    }

    //Force of gravity
    public Vector2 Gravity(Vector3 _position, float _invincible)
    {
        Vector2 _gravity = Vector2.zero;                                //Set empty vector
        for (int i = 0; i < _mainScript._gravitySources.Count; i++)     //For each Gravity Source
        {
                _gravity += ((Vector2)_mainScript._gravitySources[i].transform.position - (Vector2)_position).normalized    //Direction towards source
                * _mainScript._gravitySources[i].GetComponent<GravityScript>()._gravity                                     //Force of gravity
                / ((Vector2)_mainScript._gravitySources[i].transform.position - (Vector2)_position).magnitude;              //Length to source
        }
        if (_invincible > 0)
            return _gravity * (1-_invincible/_mainScript._invincible*1.25f); //Lessens force on ship while invincibility is active
        else
            return _gravity;    //Return vector
    }

    //Set direction of sprite to follow velocity
    public Vector3 SetDirection(Vector2 _velocity)
    {
        float _direction = Vector2.Angle(Vector2.right, _velocity);  //Calculate angle of velocity
        if (_velocity.y < 0) _direction = -_direction;               //Check if angle is negative
        return new Vector3(0, 0, _direction);                        //Rotate with movement direction
    }

    //Create trail
    public void CreateTrail(Vector3 _position, Vector3 _prevPos, int _player, ref List<GameObject> _trailList, GameObject _parent, GameObject _trailPool)
    {
        //Trail
        GameObject _trail = new GameObject();                                                               //Create trail object
        _trail.name = "Trail - Player " + (_player + 1).ToString();                                         //Set name
        _trail.tag = "Trail";                                                                               //Add tag
        _trail.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Visuals/Particles/Pixel");   //Set sprite
        _trail.AddComponent<BoxCollider2D>().isTrigger = true;                                              //Add collider and set to trigger
        _trail.GetComponent<BoxCollider2D>().enabled = false;                                               //Disable trigger
        _mainScript.StrechSprite(_trail, _position, _prevPos, 30f, _mainScript._trailWidth * 4f);           //Stretch and rotate sprite
        _trail.AddComponent<TrailScript>();                                                                 //Add script
        _trail.GetComponent<TrailScript>()._player = _player;                                               //Set player
        _trail.GetComponent<TrailScript>()._lifeSpan = _mainScript._trailLifeSpan;                          //Set lifespan
        _trail.GetComponent<TrailScript>()._parent = _parent;                                               //Set parent
        _trailList.Add(_trail);                                                                             //Add to list
        _trail.transform.parent = _trailPool.transform;                                                     //Set parent
    }

    //Set Color
    public Color SetColor(int _player, bool _outOfBounds, ref float _invincible, ref float _stunned)
    {
        Color _color = _mainScript._playerColor[_player];   //Get color from mainscript
        if (_outOfBounds)       //if out of bounds
        {
            _color = new Color( //Slight Black and White effect
                _color.r * .5f + _color.g * .25f + _color.b * .25f,
                _color.r * .25f + _color.g * .5f + _color.b * .25f,
                _color.r * .25f + _color.g * .25f + _color.b * .5f,
                _color.a
                );
        }
        if (_invincible > 0)   //If ship is invincible
            _color = _color / 2f; //weaker color and alpha

        if (_stunned > 0)
        {
            _color = new Color( //Black and White effect with some color retained
               _color.r * .40f + _color.g * .30f + _color.b * .30f,
               _color.r * .30f + _color.g * .40f + _color.b * .30f,
               _color.r * .30f + _color.g * .30f + _color.b * .40f,
               _color.a
               );
        }
        return _color; //Return color
    }

    //Set color while phasing
    public Color SetColorPhasing(int _player)
    {
        Color _color = _mainScript._playerColor[_player];   //Get color from mainscript
            _color = new Color( //Slight Black and White effect if out of bounds
                _color.r,
                _color.g,
                _color.b,
                .25f
                );
        return _color;  //Return color
    }

    //On Death
    public void Death(GameObject _go, int _player, bool _destroy, ref List<GameObject> _trailList)
    {
        //Debug.Log(_go.name + " died");        //Debug
        foreach (GameObject _g in _trailList)
            Destroy(_g);                        //Destroy trail collision
        _trailList.Clear();                     //Clear list

        GameObject _explosion = new GameObject();   //Create explosion
        if (_destroy) _explosion = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/MissileExplodeSprite")); //Set animation
        else _explosion = Instantiate<GameObject>(Resources.Load<GameObject>("Prefabs/ShipExplodeSprite")); //Set animation
        _explosion.name = "Explosion";                                                      //Set name
        _explosion.transform.position = _go.transform.position;                             //Set position
        _explosion.transform.localEulerAngles = new Vector3(0,0,Random.Range(0f,360f));     //Randomize rotation
        _explosion.transform.localScale = Vector3.one * Random.Range(0.5f, .75f);           //Randomize scale
        if (_go.GetComponent<ShipScript>())                 //if ship
            _go.GetComponent<ShipScript>().Initialize();    //Initialize for reseting

        if (_destroy) Destroy(_go);                                         //Destroy object
        else { _go.SetActive(false); _mainScript._health[_player] -= 1; }   //Deactivate object
    }

    //Create sparks
    public void Sparks(Vector3 _position, Vector2 _spd, float _r, float _g, float _b, float _randomness)
    {
        float _ranDir = Random.Range(0f, 360f);         //Angle from 0-360 degrees
        float _speed = Random.Range(0f, _randomness);   //Random speed when spawning
        _mainScript._particleScript.ParticlesSparks(    //  Spawn particles
            _position,                                  //  on position
            _spd.x + Mathf.Cos(_ranDir) * _speed,       //  with speed vector from _spd
            _spd.y + Mathf.Sin(_ranDir) * _speed,       //  and speed vector from _speed
            _r, _g, _b);                                //  Color(RGB)
    }
}
