﻿using UnityEngine;
using System.Collections;

public class GravityScript : MonoBehaviour {

    public float _mass = 1;     //The mass of the body
    public float _gravity = 1;  //The current force of gravity
    public bool _planet;        //Black Hole or Planet
    public bool _inverted;      //Inverted gravity
    GameObject _ring;           //Ring to show direction of gravity and make planets easier to see
    float _ringMove;            //Variable for pulsation of ring
    MainScript _mainScript;     //MainScript
    public Sprite[] _easterEggs;//Easter Egg Sprites
    bool _easterEgg;            //If Easter Egg

	// Use this for initialization
	void Start ()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();    //Set MainScript
        _gravity = _mass;                           //Set force of gravity
        if (_planet)                                //if planet
        {   
            int _random = Random.Range(0,100);      //Dice roll
            if (_random >= 99) _easterEgg = true;   //1% chance of being Easter Egg
            if (_easterEgg)                         //if easter egg
            {
                GetComponent<SpriteRenderer>().sprite = _easterEggs[Random.Range(0, _easterEggs.Length)];   //Set sprite to random Easter Egg
            }
            else                                    //else
            { 
                GetComponent<SpriteRenderer>().color = new Color(Random.Range(.25f, .75f), Random.Range(.25f, .75f), Random.Range(.25f, .75f), 1);  //Randomize color
                _random = Random.Range(1, 3);                           //Choose random planet animation
                if (_random == 1)
                    gameObject.AddComponent<PlanetAnimationScript1>();  //Animation 1
                if (_random == 2)
                    gameObject.AddComponent<PlanetAnimationScript2>();  //Animation 2
            }
            transform.localEulerAngles = new Vector3(0,0,Random.Range(0f,360f));    //Randomize rotation
        }
        _ring = new GameObject();                                                               //Create ring
        _ring.name = name + " - Pulse";                                                         //Set name
        _ring.transform.position = new Vector3(transform.position.x, transform.position.y, 1);  //Set position
        if (_planet) _ring.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Visuals/UI/RingFancy");  //Set sprite
        else _ring.AddComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Visuals/UI/Ring");   //Set sprite
        _ring.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;      //Set color
        _ring.transform.localScale = Vector3.one*(1f/5f);                                       //Set scale
        _ring.transform.parent = transform;                                                     //Set parent
    }

    void Update()
    {
        //Nyan Mode
        if (_mainScript._nyan)                                                  //if Nyan
        {
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;      //Set Color
            _ring.GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;//Set Color of ring
        }
    }

    // Update is called once per physics step
    void FixedUpdate ()
    {
        _gravity = Mathf.Lerp(_gravity, (_inverted)?-_mass:_mass, Time.fixedDeltaTime*5f);    //Direction of gravity
        if (_planet)
        {   //Rotate around the centre of the arena
            transform.RotateAround(Vector3.zero, Vector3.forward, 10 * Time.fixedDeltaTime * (5-Vector2.Distance(transform.position, Vector2.zero)) * (_gravity / _mass));
            _ring.transform.position = new Vector3(transform.position.x, transform.position.y, 1); //Update Ring position
        }

        _ringMove -= _gravity/_mass * Time.fixedDeltaTime/4f;                       //Change timer
        if (_ringMove > .75f) _ringMove = 0;                                        //Check for upper limit
        if (_ringMove < 0) _ringMove = .75f;                                        //Check for lower limit
        _ring.transform.localScale = Vector3.one * (1f/5f) * (1+_ringMove);         //Set scale
        _ring.GetComponent<SpriteRenderer>().color =                                //Set color
            new Color(
                GetComponent<SpriteRenderer>().color.r,                             //Red
                GetComponent<SpriteRenderer>().color.g,                             //Green
                GetComponent<SpriteRenderer>().color.b,                             //Blue
                (.75f-_ringMove)/.75f);                                             //Alpha
    }
}
