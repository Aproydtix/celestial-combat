﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class KonamiScript : MonoBehaviour {

    public int _konami;
    public bool[] _up = new bool[4];        //Up "Down"
    bool[] _upInput = new bool[4];          //Up Raw Input detected
    bool[] _upPrev = new bool[4];           //Up Raw Input detected in previous frame
    public bool[] _down = new bool[4];      //Down "Down"
    bool[] _downInput = new bool[4];        //Down Raw Input detected
    bool[] _downPrev = new bool[4];         //Down Raw Input detected in previous frame
    public bool[] _left = new bool[4];      //Left "Down"
    bool[] _leftInput = new bool[4];        //Left Raw Input detected
    bool[] _leftPrev = new bool[4];         //Left Raw Input detected in previous frame
    public bool[] _right = new bool[4];     //Right "Down"
    bool[] _rightInput = new bool[4];       //Right Raw Input detected
    bool[] _rightPrev = new bool[4];        //Right Raw Input detected in previous frame
    public bool[] _a = new bool[4];         //A "Down"
    public bool[] _b = new bool[4];         //B "Down"
    GamePad.Index[] _index = new GamePad.Index[4];  //Input index

    public GameObject _ass; //AbilitySelectScript, also used to pass on variables to gameplay

	// Use this for initialization
	void Start ()
    {
        _index[0] = GamePad.Index.One;      //Player 1
        _index[1] = GamePad.Index.Two;      //Player 2
        _index[2] = GamePad.Index.Three;    //Player 3
        _index[3] = GamePad.Index.Four;     //Player 4
        _ass = GameObject.Find("MenuSettings"); //Set variable
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < 4; i++)
        {
            _upInput[i] = GamePad.GetAxis(GamePad.Axis.Dpad, _index[i]).y > .3f;    //Check input up
            _downInput[i] = GamePad.GetAxis(GamePad.Axis.Dpad, _index[i]).y < -.3f; //Check input down
            _leftInput[i] = GamePad.GetAxis(GamePad.Axis.Dpad, _index[i]).x < -.3f; //Check input left
            _rightInput[i] = GamePad.GetAxis(GamePad.Axis.Dpad, _index[i]).x > .3f; //Check input right

            if (_upInput[i] && !_upPrev[i]) _up[i] = true; else _up[i] = false;     //if input is true and previous frame it wasn't
            _upPrev[i] = _upInput[i];                                               //update prev

            if (_downInput[i] && !_downPrev[i]) _down[i] = true; else _down[i] = false; //if input is true and previous frame it wasn't
            _downPrev[i] = _downInput[i];                                               //update prev

            if (_leftInput[i] && !_leftPrev[i]) _left[i] = true; else _left[i] = false; //if input is true and previous frame it wasn't
            _leftPrev[i] = _leftInput[i];                                               //update prev

            if (_rightInput[i] && !_rightPrev[i]) _right[i] = true; else _right[i] = false; //if input is true and previous frame it wasn't
            _rightPrev[i] = _rightInput[i];                                                 //update prev

            _b[i] = GamePad.GetButtonDown(GamePad.Button.B, _index[i]);     //B input
            _a[i] = GamePad.GetButtonDown(GamePad.Button.A, _index[i]);     //A input


            //Konami ↑↑↓↓←→←→BA
            switch (_konami)
            {
                case 0: //Up
                    if (_up[i]) _konami++;                                                  //Desired button
                    break;
                case 1: //Up
                    if (_up[i]) _konami++;                                                  //Desired button
                    if (_down[i] || _left[i] || _right[i] || _b[i] || _a[i]) _konami = 0;   //Breaks button sequence
                    break;  
                case 2: //Down
                    if (_down[i]) _konami++;                                                //Desired button
                    if (_up[i] || _left[i] || _right[i] || _b[i] || _a[i]) _konami = 0;     //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 3: //Down
                    if (_down[i]) _konami++;                                                //Desired button
                    if (_up[i] || _left[i] || _right[i] || _b[i] || _a[i]) _konami = 0;     //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 4: //Left
                    if (_left[i]) _konami++;                                                //Desired button
                    if (_down[i] || _up[i] || _right[i] || _b[i] || _a[i]) _konami = 0;     //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 5: //Right
                    if (_right[i]) _konami++;                                               //Desired button
                    if (_down[i] || _left[i] || _up[i] || _b[i] || _a[i]) _konami = 0;      //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 6: //Left
                    if (_left[i]) _konami++;                                                //Desired button
                    if (_down[i] || _up[i] || _right[i] || _b[i] || _a[i]) _konami = 0;     //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 7: //Right
                    if (_right[i]) _konami++;                                               //Desired button
                    if (_down[i] || _left[i] || _up[i] || _b[i] || _a[i]) _konami = 0;      //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 8: //B
                    if (_b[i]) _konami++;                                                   //Desired button
                    if (_down[i] || _left[i] || _right[i] || _up[i] || _a[i]) _konami = 0;  //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
                case 9: //A
                    if (_a[i])                                                              //Desired button
                    {
                        _ass.GetComponent<AbilitySelectScript>()._nyan = !_ass.GetComponent<AbilitySelectScript>()._nyan;   //Set variable in ASS
                        Camera.main.GetComponent<MainMenu>()._nyan = _ass.GetComponent<AbilitySelectScript>()._nyan;        //Set variable in Main Menu
                        _konami = 0;                                                                                        //Resets button sequence
                    }
                    if (_down[i] || _left[i] || _right[i] || _b[i] || _up[i]) _konami = 0;  //Breaks button sequence
                    if (_up[i]) _konami = 1;                                                //Breaks button sequence, but uses first input
                    break;
            }
        }
    }
}
