﻿using UnityEngine;
using System.Collections;

public class FindInactiveMainMenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>()._mainMenu = gameObject;
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {

        
	}
}
