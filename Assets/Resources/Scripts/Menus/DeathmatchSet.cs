﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathmatchSet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>()._gameMode = AbilitySelectScript.GameMode.Deathmatch;
        GetComponent<SurvivalSet>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
