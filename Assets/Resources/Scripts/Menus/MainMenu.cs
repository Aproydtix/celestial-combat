﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public bool _nyan;
    bool _nyanPrev;
    public GameObject _nyanCat;

	// Use this for initialization
	void Start ()
    {
        _nyan = GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>()._nyan;
        _nyanPrev = _nyan;
        if (_nyan)
        {
            GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Nyan/BGM_Nyan1");
            _nyanCat.SetActive(true);
        }
        else
        {
            GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Menu/BGM_Menu3");
            _nyanCat.SetActive(false);
        }
        GetComponent<AudioSource>().Play();
    }
	
	// Update is called once per frame
	void Update ()
    {


	    if (_nyan != _nyanPrev)
        {
            if (_nyan)
            {
                GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Nyan/BGM_Nyan1");
                _nyanCat.SetActive(true);
            }
            else
            {
                GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/bgm/BGM_Menu/BGM_Menu3");
                _nyanCat.SetActive(false);
            }
            GetComponent<AudioSource>().Play();
            _nyanPrev = _nyan;
        }
	}
}
