﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using GamepadInput;

public class DisplayToggleSelect : MonoBehaviour
{
    //Toggle display options

    public Toggle isColorBlindToggle;
    public Toggle isParticlesToggle;
    public Toggle isAntiAliasingToggle;

    public void ActiveToggle ()
    {         //checks active toggle
        if (isColorBlindToggle.isOn)
        {
            Debug.Log("Color blind mode is turned on.");//if colorblind toggle is active
        }

        if (isParticlesToggle.isOn)
        {
            Debug.Log("Particles is turned on.");//if particles toggle is active
        }

        if (isAntiAliasingToggle.isOn)
        {
            Debug.Log("Anti - aliasing is turned on.");//if anti-aliasing toggle is active
        }
    }

    public void onSubmit ()
    { //checks active toggle
        ActiveToggle();
    }
}
