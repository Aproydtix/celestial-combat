﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivalSet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.Find("MenuSettings").GetComponent<AbilitySelectScript>()._gameMode = AbilitySelectScript.GameMode.Survival;
        GetComponent<SurvivalSet>().enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
