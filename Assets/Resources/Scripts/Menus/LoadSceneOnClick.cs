﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using GamepadInput;

public class LoadSceneOnClick : MonoBehaviour
{

    public void LoadByIndex(int sceneIndex)
    {//singleplayer saved as scene 1, multiplayer local as scene 2, multiplayer online as scene 3
        //putting temporary level 1, 2 and 3 as scene 4, 5 and 6 respectively
        SceneManager.LoadScene(sceneIndex);//loads a scene when called from a click (I.E. when game starts)
    }
}
