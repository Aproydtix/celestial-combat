﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using GamepadInput;

public class SelectOnInput : MonoBehaviour
{
    public EventSystem eventSystem;
    public GameObject selectedObject;

    public bool buttonSelected;
    // Use this for initialization
    void Start()
    {
        buttonSelected = false;
    }

    // Update is called once per frame
    //checks for button input from keyboard/ gamepad (update: only gamepad)
    void Update()
    {
        if (Input.GetAxisRaw ("L_YAxis_1") !=0 && buttonSelected==false)
        {
            eventSystem.SetSelectedGameObject(selectedObject);
            buttonSelected = true;
        }
    }
    private void OnDisable()
    {
        buttonSelected = false;
    }
}