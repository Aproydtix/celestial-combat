﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class QuitOnClick : MonoBehaviour
{
    public void Quit ()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;//quits from editor
#else
        Application.Quit ();    //quits from running application, if editor is not in use
#endif
    }
}