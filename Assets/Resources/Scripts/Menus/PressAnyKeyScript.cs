﻿using UnityEngine;
using System.Collections;
using GamepadInput;

public class PressAnyKeyScript : MonoBehaviour
{
    public GameObject _mainMenu;

    void Update()
    {
        if (GamePad.GetButton(GamePad.Button.Start, GamePad.Index.Any))
        {
            _mainMenu.SetActive(true);
            gameObject.SetActive(false);
            //Debug.Log("A key or mouse click has been detected");
        }
    }
}