﻿using UnityEngine;
using System.Collections;

public class MineAnimationScript : MonoBehaviour
{
    public int Mfps = 1;        // frames per second
    public float index;         

    public Sprite[] sprite = new Sprite[1];

    void Start()
    {
        sprite = Resources.LoadAll<Sprite>("Visuals/Abilities/MineNotSheet");       //sprite location
    }

    void Update()
    {
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second

        if (index >= 1) index = 0;      //repeats when all frames have been used

        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite
    }
}