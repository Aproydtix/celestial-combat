﻿using UnityEngine;
using System.Collections;

public class MissileExplosionAnimationScript : MonoBehaviour {

    public int Mfps = 71;                       //Frames per second
    public float index;                         //Sprite Index
    public Sprite[] sprite = new Sprite[71];    //Sprites in animation
    MainScript _mainScript;                     //MainScript

    void Start()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();                //Set MainScript
        sprite = Resources.LoadAll<Sprite>("Visuals/Explosions/MissileExplosionSheet");         //Load Animation
        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/sfx/SFX_Explosion" + Random.Range(1,3).ToString()); //Set AudioClip
        GetComponent<AudioSource>().pitch = Random.Range(1.1f, 1.25f);                          //Randomize Pitch
        GetComponent<AudioSource>().Play();                                                     //Play sound
    }

    void Update()
    {
        if (_mainScript != null && _mainScript._nyan)                               //Checks MainScript for Nyan
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;          //Set Color

        index += Time.deltaTime * Mfps;                                             //Sets frames per second
        if (index >= sprite.Length || _mainScript == null)                          //Checks if the animation is done or MainScript does not exist
        {
            Destroy(gameObject);                                                    //Destroy when all frames have been used
            return;                                                                 //Return, as the object is being destroyed
        }    
        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];    //Sets Sprite
    }
}
