﻿using UnityEngine;
using System.Collections;

public class AsteroidAnimationScript2 : MonoBehaviour
{
    public int MuvTileY = 3;   //texture sheet column
    public int MuvTileX = 5;   //texture sheet row

    public int Mfps = 30;        // frames per second
    public float index;

    public Sprite[] sprite = new Sprite[61];

    void Start()
    {
        sprite = Resources.LoadAll<Sprite>("Visuals/Celestial Bodies/AsteroidAnimationSheet2");       //sprite location
    }

    void Update()
    {
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second

        if (index >= 61) index = 0;      //repeats when all frames have been used

        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite


    }
}