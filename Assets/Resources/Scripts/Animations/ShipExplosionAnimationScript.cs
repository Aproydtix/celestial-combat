﻿using UnityEngine;
using System.Collections;

public class ShipExplosionAnimationScript : MonoBehaviour
{
    public int Mfps = 8;
    public float index;
    public Sprite[] sprite = new Sprite[14];
    MainScript _mainScript;     //MainScript

    void Start()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();
        sprite = Resources.LoadAll<Sprite>("Visuals/Explosions/ShipExplosionSheet");       //sprite location
        GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Audio/sfx/SFX_Explosion" + Random.Range(1, 3).ToString());
        GetComponent<AudioSource>().pitch = Random.Range(1.1f, 1.25f);
        GetComponent<AudioSource>().Play();
    }

    void Update()
    {
        if (_mainScript != null && _mainScript._nyan)
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second
        if (index >= 14 || _mainScript == null) { Destroy(gameObject); return; }      //repeats when all frames have been used
        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite



        /* //split into horizontal and vertical indexes
         var uIndex = index % uvTileX;
         var vIndex = index / uvTileX;

         //repeat when exhausted all frames, should we want such a thing
         /*index = index % (uvTileY * uvTileX);*

         //size of each tile
         Vector2 size = new Vector2(1.0F / uvTileY, 1.0F / uvTileX);

         //build the offset
         //v coordinate is at the bottom of the image in openGL, so we invert it
         Vector2 offset = new Vector2(uIndex * size.x, 1.0F - size.y - vIndex * size.y);

         GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
         GetComponent<Renderer>().material.SetTextureScale("_MainTex", size);*/
    }
}
