﻿using UnityEngine;
using System.Collections;

public class WarpAnimationScript : MonoBehaviour
{
    public int Mfps = 30;        // frames per second
    public float index;
    MainScript _mainScript;
    public Sprite[] sprite = new Sprite[25];

    void Start()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();
        sprite = Resources.LoadAll<Sprite>("Visuals/Abilities/WarpSheet");       //sprite location
    }

    void Update()
    {
        if (_mainScript._nyan)
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second

        //if (index >= 25) index = 0;      //repeats when all frames have been used
        if (index >= 25)
        {
            Destroy(gameObject);
            return;
        }

        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite


    }
}