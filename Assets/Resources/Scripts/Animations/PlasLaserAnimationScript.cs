﻿using UnityEngine;
using System.Collections;

public class PlasLaserAnimationScript : MonoBehaviour
{
    public int MuvTileY = 1;   //texture sheet column
    public int MuvTileX = 10;   //texture sheet row

    public int Mfps = 10;        // frames per second

    public float index;

    public Sprite[] sprite = new Sprite[10];

    void Start()
    {
        sprite = Resources.LoadAll<Sprite>("Visuals/Abilities/PlasLaserSheet");       //sprite location
    }

    void Update()
    {
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second

        if (index >= 10) index = 0;      //repeats when all frames have been used

        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite
    }
}
