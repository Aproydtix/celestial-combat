﻿using UnityEngine;
using System.Collections;

public class StunAnimationScript : MonoBehaviour
{

    public int MuvTileY = 7;   //texture sheet column
    public int MuvTileX = 1;   //texture sheet row

    public int Mfps = 15;        // frames per second

    public float index;

    public Sprite[] sprite = new Sprite[6];

    void Start()
    {
        sprite = Resources.LoadAll<Sprite>("Visuals/Abilities/StunSheet");       //sprite location
    }

    void Update()
    {
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second

        if (index >= 6) index = 0;      //repeats when all frames have been used

        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];

        //transform.localScale = new Vector3(1, 1, 0);       //scales the sprite



        /*//split into horizontal and vertical indexes
        var uIndex = index % MuvTileX;
        var vIndex = index / MuvTileX;

        //repeat when exhausted all frames, should we want such a thing
        /*index = index % (MuvTileY * MuvTileX);*

        //size of each tile
        Vector2 size = new Vector2(1.0F / MuvTileY, 1.5F / MuvTileX);

        //build the offset
        //v coordinate is at the bottom of the image in openGL, so we invert it
        Vector2 offset = new Vector2(uIndex * size.x, 1.0F - size.y - vIndex * size.y);

        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", offset);
        GetComponent<Renderer>().material.SetTextureScale("_MainTex", size);*/
    }
}
