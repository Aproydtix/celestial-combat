﻿using UnityEngine;
using System.Collections;

public class GravityReverseAnimationScript : MonoBehaviour
{
    public int Mfps = 50;        // frames per second
    public float index;
    public Sprite[] sprite = new Sprite[22];
    MainScript _mainScript;
    public int _player;

    void Start()
    {
        _mainScript = GameObject.Find("Main Camera").GetComponent<MainScript>();
        sprite = Resources.LoadAll<Sprite>("Visuals/Abilities/GravityReverseSheet");       //sprite location
    }

    void Update()
    {
        if (_mainScript._nyan)
            GetComponent<SpriteRenderer>().color = _mainScript._nyanColor;
        //calculate the index
        index += Time.deltaTime * Mfps;      //sets frames per second
        if (index >= 22 || !_mainScript._playerShips[_player].active)
        {
            Destroy(gameObject);
            return;
        }
        GetComponent<SpriteRenderer>().sprite = sprite[Mathf.FloorToInt(index)];
        //GetComponent<SpriteRenderer>().color = _mainScript._playerColor[_player];
        transform.parent = _mainScript._playerShips[_player].transform;
        transform.position = _mainScript._playerShips[_player].transform.position;
        
    }
}